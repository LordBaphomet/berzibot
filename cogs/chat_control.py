from typing import Dict, Optional, Union
import discord
from discord import NotFound
from discord.ext.commands import CheckFailure, Cog, has_permissions
from berzibot import Berzibot
from components.berzicommand import berzi_command, berzi_group
from components.context import BerziContext as Context
from components.localisation import fetch_line
from components.log import logger
from config import DEFAULT_DELETE_AFTER


def setup(bot: Berzibot):
    bot.add_cog(ChatControl())


class ChatControl(Cog):
    """Hold all commands for controlling chat and channels."""

    @has_permissions(manage_messages=True)
    @berzi_command(name="embed", meta={"mod_only": True})
    async def embed(self, ctx: Context, title: str, *, content: str):
        """Embed some text for better visibility."""
        embed = discord.Embed()

        quotes = "'\""
        content = (content or "").strip(quotes)
        title = (title or "").strip(quotes)

        if not (content and title):
            await ctx.send(fetch_line("user error"))
            return

        if not (len(content) <= 1024 and len(title) <= 256) or len(embed) > 6000:
            await ctx.send(fetch_line("too long"))
            return

        embed.add_field(name=title, value=content, inline=False)

        await ctx.message.delete()
        await ctx.send(embed=embed)

    @has_permissions(manage_channels=True)
    @berzi_command(name="announce", meta={"mod_only": True})
    async def announce(self, ctx: Context, content: str, *, roles: str = None):
        """Make an announcement in this channel, optionally pinging roles (as IDs or names, separated by comma).
        Wrap the content in quotes.
        This allows to ping roles that cannot normally be pinged.
        """

        to_mention: Dict[discord.Role, bool] = {}
        """Store roles and whether they can usually be mentioned."""

        if roles:
            guild_roles = {role.name.lower(): role for role in ctx.guild.roles}
            for role_name in roles.split(","):
                role_name = role_name.lower().strip()
                if role_name.isnumeric():
                    if role := ctx.guild.get_role(int(role_name)):
                        to_mention.update({role: role.mentionable})
                        await role.edit(mentionable=True)
                else:
                    if role_name in guild_roles.keys():
                        role = guild_roles[role_name]
                        to_mention.update({role: role.mentionable})
                        await role.edit(mentionable=True)

        mention_string = " ".join(role.mention for role in to_mention.keys())
        announcement = "\n".join((content, mention_string))

        await ctx.message.delete()
        await ctx.send(announcement)

        # Restore original mentionability (if that's a word).
        for role, mentionable in to_mention.items():
            await role.edit(mentionable=mentionable)

        logger.info(
            f"{ctx.author} made an announcement in {ctx.channel} pinging {to_mention.keys()}."
        )

    @berzi_group(name="channel", meta={"mod_only": True})
    async def channel(self, ctx: Context):
        """Commands for text channel administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "channel"))

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.berzi_command(name="flush", meta={"mod_only": True})
    async def flush(
        self,
        ctx: Context,
        channel: Optional[discord.TextChannel] = None,
        member: Optional[Union[discord.Member, discord.User]] = None,
    ):
        """Remove all messages from a channel, optionally only by a specific user."""
        # Allow specifying a member without a channel:
        if not member and (
            isinstance(channel, discord.Member) or isinstance(channel, discord.User)
        ):
            member = channel
            channel = None

        if (
            member
            and not member.bot
            and member.guild_permissions <= ctx.author.guild_permissions
        ):
            raise CheckFailure

        channel = channel or ctx.channel

        def check(message: discord.Message) -> bool:
            return message.author == member

        async with ctx.typing():
            try:
                await channel.purge(check=check if member else None)
            except NotFound:
                pass

        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        for_member = f" for {member=}" if member else ""
        logger.info(
            f"{ctx.author} flushed {channel.name=} on {ctx.guild.name=}{for_member}."
        )

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.berzi_command(name="truncate", meta={"mod_only": True})
    async def truncate(
        self, ctx: Context, direction: str, start: discord.Message,
    ):
        """Truncate all messages `before` or `after` a certain message ID (included)."""
        direction = direction.lower()

        if direction not in ("before", "after"):
            raise discord.InvalidArgument

        def check(message: discord.Message) -> bool:
            return (
                message.created_at <= start.created_at
                if direction == "before"
                else message.created_at >= start.created_at
            )

        channel = ctx.channel

        async with ctx.typing():
            await channel.purge(check=check)
        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        logger.info(
            f"{ctx.author} truncated {channel.name=} on {ctx.guild.name=} "
            f"{direction} {start.created_at}."
        )
