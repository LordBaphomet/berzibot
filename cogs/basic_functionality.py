from datetime import datetime
from inspect import Parameter
from pathlib import Path
from typing import Literal, OrderedDict, Tuple
import discord
from discord.ext.commands import (
    Cog,
    ExtensionAlreadyLoaded,
    ExtensionFailed,
    ExtensionNotFound,
    ExtensionNotLoaded,
    has_permissions,
    is_owner,
    NoEntryPointError,
)
from berzibot import Berzibot
from components.berzicommand import berzi_command, berzi_group, BerziCommand
from components.context import BerziContext as Context
from components.localisation import fetch_line
from components.log import logger
from components.utils import (
    EmbedPaginator,
    from_snake_case,
    paginate_string,
    to_snake_case,
)
from config import DEFAULT_DELETE_AFTER


def setup(bot: Berzibot):
    bot.add_cog(BasicFunctionality(bot))


class BasicFunctionality(Cog, command_attrs=dict(hidden=True)):
    """Hold all basic functionality."""

    def __init__(self, bot: Berzibot):
        self.bot = bot

    @Cog.listener()
    async def on_ready(self):
        logger.info("Connected.")

    @Cog.listener()
    async def on_disconnect(self):
        logger.info("Disconnected.")

    @berzi_command(name="help", hidden=False)
    async def help(self, ctx: Context, *, for_command: str = None):
        """Display commands and their usage."""
        if for_command:
            for_command = for_command.lower().strip()

        embed = discord.Embed()
        embed.description = fetch_line("instructions")
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.colour = self.bot.user.colour
        embed.title = for_command or "Help"

        no_help = fetch_line("no help")

        fields = set()
        for cmd in self.bot.walk_commands():
            cmd: BerziCommand

            if for_command and cmd.name != for_command:
                continue

            if not for_command and (
                cmd.hidden or isinstance(cmd, discord.ext.commands.Group)
            ):
                continue

            # Hide mod-only commands from users who don't have appropriate permissions.
            if not await self.bot.is_owner(ctx.author):
                if (
                    cmd.get_meta("mod_only")
                    and not ctx.author.guild_permissions.manage_channels
                ) or (
                    cmd.get_meta("admin_only")
                    and not ctx.author.guild_permissions.administrator
                ):
                    continue

            try:
                params: OrderedDict[str, Parameter] = cmd.clean_params
            except ValueError:
                logger.error(
                    f"Command {cmd} is missing context param and won't be shown in list."
                )
                continue

            signature_bits = []
            for param in params.values():
                if param.default != Parameter.empty:
                    paren_open = "("
                    paren_closed = ")"
                else:
                    paren_open = ""
                    paren_closed = ""

                signature_bits.append(f"`{paren_open}{param.name}{paren_closed}`")

            signature = " ".join(signature_bits)

            name = (
                f"{'🚫 ' if not cmd.enabled else ''}" f"{cmd.qualified_name} {signature}"
            )

            help_text = (cmd.help if for_command else cmd.short_doc) or no_help

            fields.add((name, help_text))

        if not fields and for_command:
            await ctx.send(fetch_line("fail find", for_command))
            return

        fields_list = sorted(list(fields), key=lambda tp: tp[0])

        paginator = EmbedPaginator(
            ctx,
            fields_list,
            thumbnail=self.bot.user.avatar_url,
            title=f"Help for {for_command or 'my commands'}",
            description="*Ping me with a command!* Parameters in (parentheses) are optional.",
            page_size=5,
        )
        await paginator.paginate()

    @berzi_command(name="ping", hidden=False)
    async def ping(self, ctx: Context):
        """Ping the bot to check response time."""
        delay = (ctx.message.created_at - datetime.now()).microseconds / 1000
        await ctx.send(f"Pong! 🏓 {delay:.2f}ms.")

    @is_owner()
    @berzi_command(name="die", hidden=True)
    async def die(self, ctx: Context):
        """Kill me. :("""
        await ctx.send(fetch_line("logout"))
        await self.bot.close()

    @is_owner()
    @berzi_command(name="sleep", hidden=True)
    async def sleep(self, ctx: Context):
        """Go to sleep, ignoring all commands except the wake up call."""
        if not self.bot.sleeping:
            await self.bot.set_sleeping(True)
            await ctx.send(fetch_line("goodnight"))

    @is_owner()
    @berzi_command(name="wake", hidden=True)
    async def wake(self, ctx: Context, up: str = None):
        """Wake up after being put to sleep."""
        if self.bot.sleeping:
            # Memeily force correct English, lul.
            if up.lower().strip() == "up":
                await self.bot.set_sleeping(False)
                await ctx.send(fetch_line("wake up"))

    @is_owner()
    @berzi_group(name="errors", aliases=["error"], hidden=True)
    async def errors(self, ctx: Context):
        """Commands for error reporting and handling."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "errors"))

    @is_owner()
    @errors.berzi_command(name="send", hidden=True)
    async def errors_send(self, ctx: Context):
        currently_sending = self.bot.sending_errors
        self.bot.set_sending_errors(not currently_sending)
        await ctx.send(
            f"Ok,{'' if currently_sending else ' not'} sending errors from now on.",
            delete_after=DEFAULT_DELETE_AFTER,
        )

    @is_owner()
    @errors.berzi_command(name="logs", aliases=["log"], hidden=True)
    async def errors_log(self, ctx: Context):
        """Send the current logs."""
        try:
            with open("../discord.log", "r") as fd:
                message = fd.read()
        except FileNotFoundError:
            message = "Couldn't find the file!"

        async with ctx.author.create_dm() as dm_channel:
            for part in paginate_string(
                message, 2000, separator="\n", enforce_length=True,
            ):
                await dm_channel.send(part)

    @is_owner()
    @berzi_group(name="cog", hidden=True)
    async def cog(self, ctx: Context):
        """Commands for cog administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "cog"))

    def _list_cogs(self) -> Tuple[set, set]:
        """Return a set of already loaded cogs and one of available cogs."""
        already_loaded = {to_snake_case(cog_name) for cog_name in self.bot.cogs.keys()}

        available = {
            cog.name[:-3]
            for cog in Path("cogs/").glob("*.py")
            if not cog.name.startswith("_")
        }

        return already_loaded, available

    async def _manage_cogs(
        self,
        ctx: Context,
        cog_names: str,
        operation: Literal["load", "unload", "reload"],
    ):
        operation = operation.lower()
        if operation not in ("load", "unload", "reload"):
            raise ValueError(f"Unknown operation: {operation}.")

        already_loaded, available = self._list_cogs()
        this_cog = {__name__.split(".")[-1]}

        if cog_names.lower() == "all":
            to_process = available
        else:
            cog_names = cog_names.split(",")
            to_process = {to_snake_case(name) for name in cog_names}

        if operation == "load":
            to_process = to_process.difference(already_loaded)
        elif operation == "reload":
            to_process = to_process.intersection(already_loaded)
        elif operation == "unload":
            to_process = to_process.intersection(already_loaded).difference(this_cog)

        if not to_process:
            await ctx.send(fetch_line("successful"))
            return

        processed = 0
        not_found = 0
        for cog_name in to_process:
            try:
                getattr(self.bot, f"{operation}_extension")(f"cogs.{cog_name}")
                logger.info(f"{operation.capitalize()}ed cog {cog_name}.")
                processed += 1
            except (ExtensionAlreadyLoaded, NoEntryPointError, ExtensionFailed) as e:
                logger.info(f"Failed to load {cog_name=}. {e}")
            except (ExtensionNotFound, ExtensionNotLoaded) as e:
                not_found += 1
                logger.info(f"Failed to find {cog_name=}. {e}")

        if processed:
            await ctx.send(fetch_line("successful"))
        else:
            await ctx.send(fetch_line("fail"))

        if not_found:
            await ctx.send(fetch_line("fail find", f"{not_found} of them"))

    @cog.berzi_command(name="load", hidden=True)
    async def load(self, ctx: Context, *, cog_names: str):
        """Load a cog."""
        await self._manage_cogs(ctx, cog_names, "load")

    @cog.berzi_command(name="unload", hidden=True)
    async def unload(self, ctx: Context, *, cog_names: str):
        """Unload a cog."""
        await self._manage_cogs(ctx, cog_names, "unload")

    @cog.berzi_command(name="reload", hidden=True)
    async def reload(self, ctx: Context, *, cog_names: str):
        """Reload a cog."""
        await self._manage_cogs(ctx, cog_names, "reload")

    @cog.berzi_command(name="list", hidden=True)
    async def list(self, ctx: Context):
        """List all available and loaded cogs."""
        loaded, available = self._list_cogs()

        def format_cog(cog_name: str) -> str:
            icon = "🏳️" if cog_name in loaded else "🏴"
            cog_name = from_snake_case(cog_name)

            return f"{icon} {cog_name}"

        message = f"Available cogs:\n"
        message += "\n".join((format_cog(cog) for cog in available))

        await ctx.send(message)

    @has_permissions(manage_emojis=True)
    @berzi_group(name="emoji")
    async def emoji(self, ctx: Context):
        """Commands for emoji management."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "emoji"))

    @emoji.berzi_command(name="sort")
    async def sort_emoji(self, ctx: Context, *, order: str):
        """Sort the server's emoji. Do `emoji sort help` for instructions."""
        order = order.strip()
        all_emoji = ctx.guild.emojis

        sort_commands = ("alphadesc", "alphaasc", "reverse")
        is_emoji_list = "[" in order and "]" in order
        is_sort_command = order.lower() in sort_commands
        if order.lower() == "help":
            message_parts = (
                "**Usage:** provide one of "
                + ", ".join(f"`{c}`" for c in sort_commands)
                + " or give me a list of emoji in the order you want.\n"
                "Here are all the emoji of the server; if you don't write all of them,"
                " I'm going to put all missing ones at the end. Do include the brackets."
                " Names are case-sensitive.",
                "```\n[" + ",".join(emoji.name for emoji in all_emoji) + "]\n```",
                "**Remember:** in the server's emoji settings, the emoji on top are the"
                " closest candidates to getting removed if there are too few slots,"
                " so I order them upside-down.",
                "**Please backup your emoji before running this command.**\n"
                "If you use this on more than a handful of emoji, Discord's API limit"
                " will be reached, so it will take several hours to finish the job"
                " completely (even after I say I'm done). Some emoji may be missing"
                " during that time.",
            )
            for part in message_parts:
                await ctx.send(part)
            return

        emoji_list = []

        if is_sort_command:
            emoji_list = all_emoji.copy()
            order = order.lower()

            if order == "reverse":
                emoji_list.reverse()
            else:
                emoji_list.sort(
                    key=lambda emoji: emoji.name, reverse=order.endswith("desc")
                )

        if is_emoji_list:
            emoji_by_name = {emoji.name: emoji for emoji in all_emoji}

            names = [
                name
                for emoji_name in order.strip("[]").split(",")
                if (name := emoji_name.strip()) in emoji_by_name
            ]

            final_emoji = []
            for name in names:
                final_emoji.append(emoji_by_name[name])
                del emoji_by_name[name]

            emoji_list = final_emoji + list(emoji_by_name.values())

        await ctx.send(
            f"Ok. Let me collect the {len(emoji_list)} emoji.\n"
            f"This will take a bit. Don't touch the emoji until I'm finished."
        )

        emoji_data = []
        for emoji in emoji_list:
            emoji_data.append((emoji.name, await emoji.url.read()))
            await emoji.delete(
                reason=f"Emoji sort command on Berzibot by {ctx.author}."
            )

        await ctx.send("Uploading new order...")
        guild = ctx.guild
        for name, image in emoji_data:
            await guild.create_custom_emoji(name=name, image=image)

        await ctx.send(fetch_line("successful"))
