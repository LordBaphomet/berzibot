import io
import time
import traceback
from discord import Guild, Member, RawReactionActionEvent, TextChannel
from discord.ext.commands import Cog, is_owner
from berzibot import Berzibot
from cogs.thing_of_the_day import ThingFormatter
from components import utils
from components.berzicommand import berzi_command
from components.context import BerziContext as Context
from components.utils import (
    cleanup_code,
    current_timeofday,
    format_table,
    time_converter,
)


def setup(bot: Berzibot):
    bot.add_cog(Testing(bot))


class Testing(Cog, command_attrs=dict(hidden=True)):
    """Hold all commands for testing purposes."""

    def __init__(self, bot: Berzibot):
        self.bot = bot

    # @Cog.listener()
    # async def on_raw_reaction_add(self, payload: RawReactionActionEvent):
    #     if (
    #         payload.message_id != 829007815061274684
    #         or payload.event_type != "REACTION_ADD"
    #         or payload.member.bot
    #     ):
    #         return
    #
    #     roles = {
    #         "✔": 670675561986916362,
    #         "❌": 670675628026232852,
    #     }
    #
    #     channel: TextChannel = self.bot.get_channel(payload.channel_id)
    #     member: Member = payload.member
    #     guild: Guild = self.bot.get_guild(payload.guild_id)
    #
    #     if payload.emoji:
    #         role = guild.get_role(roles.get(payload.emoji.name, 0))
    #         if role is None:
    #             await channel.send(f"Nope. {payload.emoji}")
    #         else:
    #             await member.add_roles(role, atomic=True)
    #
    #     message = await channel.fetch_message(payload.message_id)
    #     await message.remove_reaction(payload.emoji, payload.member)

    @is_owner()
    @berzi_command(name="rolepost")
    async def rolepost(self, ctx: Context):
        message = await ctx.send("Here, react to this.")
        await message.add_reaction("✔")
        await message.add_reaction("❌")

    @is_owner()
    @berzi_command(name="echo", hidden=True)
    async def echo(self, ctx: Context, content: str = ""):
        """Repeat what was sent as argument."""
        await ctx.send(content)

    @is_owner()
    @berzi_command(name="server_time")
    async def server_time(self, ctx: Context):
        """Return server time as used by any of the bot's time-sensitive operations."""
        await ctx.send(current_timeofday().isoformat())

    @is_owner()
    @berzi_command(name="time")
    async def time(self, ctx: Context, *, content: time_converter):
        """Debug the time_converter."""
        await ctx.send(content)

    @is_owner()
    @berzi_command(name="publish_totd")
    async def publish_totd(self, ctx: Context):
        """Publish all Things of the Day scheduled for the current minute, if any."""
        current_timeofday_ = utils.current_timeofday()

        async with self.bot.pool.acquire() as conn:
            entries = await conn.fetch(
                """
SELECT
    e.id,
    qualifier,
    content,
    extra,
    t.server,
    t.channel,
    (
        SELECT COUNT(*) FROM totd_mentions m
        WHERE m.entry_id=e.id
        AND m.channel=t.channel
        AND m.server=t.server
    ) AS appeared
FROM totd_entries e JOIN totd_tasks t ON t.language_id=e.language_id
WHERE is_active=true
AND extract(hour FROM send_at at time zone 'utc')::int=$1
AND extract(minute FROM send_at at time zone 'utc')::int=$2
ORDER BY e.language_id, appeared, random();
                """,
                current_timeofday_.hour,
                current_timeofday_.minute,
            )

        for entry_id, qualifier, content, extra, server, channel in entries:
            await conn.execute(
                """
INSERT INTO totd_mentions
(entry_id, channel, server)
VALUES($1, $2, $3);
                """,
                entry_id,
                channel,
                server,
            )

            guild = self.bot.get_guild(server)
            location = guild.get_channel(channel)

            embed = ThingFormatter.embed_entry(qualifier, content, extra, guild)
            embed.colour = self.bot.user.colour

            await location.send(embed=embed)
            await ctx.send(embed=embed)

    @is_owner()
    @berzi_command(hidden=True)
    async def sql(self, ctx, *, query: str):
        """Run some SQL."""
        query = cleanup_code(query)

        is_multistatement = query.count(";") > 1
        if is_multistatement:
            # fetch does not support multiple statements
            strategy = ctx.db.execute
        else:
            strategy = ctx.db.fetch

        # noinspection PyBroadException
        try:
            start = time.perf_counter()
            results = await strategy(query)
            dt = (time.perf_counter() - start) * 1000.0
        except Exception:
            return await ctx.send(f"```py\n{traceback.format_exc()}\n```")

        rows = len(results)
        if is_multistatement or rows == 0:
            return await ctx.send(f"`{dt:.2f}ms: {results}`")

        render = format_table(results)

        fmt = f"```\n{render}\n```\n*Returned row(s) in {dt:.2f}ms*"
        if len(fmt) > 2000:
            async with ctx.bot.session.post(
                "http://0x0.st", data={"file": io.StringIO(fmt)}
            ) as r:
                await ctx.send(f"Too many results: {await r.text()}")
                return

        await ctx.send(fmt)
