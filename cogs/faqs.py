from typing import List, Optional, Union
import asyncpg
from discord.ext.commands import (
    Cog,
    Converter,
    MissingPermissions,
)
from berzibot import Berzibot, BerziContext
from components.berzicommand import berzi_command, berzi_group
from components.localisation import easter_eggs, fetch_easter_egg, fetch_line
from enum import Enum, auto
from asyncpg import Record, UniqueViolationError
import discord
from components.utils import EmbedPaginator, paginate_string
import re


def setup(bot: Berzibot):
    bot.add_cog(FAQs(bot))


class FAQPermission(Converter, Enum):
    NONE = auto()
    """No permissions. Not present in db-side enum. Used to signal removal of a role."""

    ALIASES = auto()
    """Can create FAQ names."""

    CREATE = auto()
    """Can create FAQs (and edit and delete own FAQs) in addition to previous permissions."""

    MODIFY = auto()
    """Can modify any FAQ in addition to previous permissions."""

    DELETE = auto()
    """Can delete any FAQ in addition to previous permissions."""

    PERMISSIONS = auto()
    """Can assign and modify role permissions in addition to previous permissions."""

    def __ge__(self, other):
        if other.__class__ is self.__class__ and self.value >= other.value:
            return True

        return False

    def __gt__(self, other):
        if other.__class__ is self.__class__ and self.value > other.value:
            return True

        return False

    def __le__(self, other):
        if other.__class__ is self.__class__ and self.value <= other.value:
            return True

        return False

    def __lt__(self, other):
        if other.__class__ is self.__class__ and self.value < other.value:
            return True

        return False

    def __eq__(self, other):
        if other.__class__ is self.__class__ and self.value is other.value:
            return True

        return False

    async def convert(self, ctx: BerziContext, argument: str) -> "FAQPermission":
        return self.from_db_name(argument)

    def to_db_name(self) -> str:
        return self.name.lower()

    @classmethod
    def from_db_name(cls, name: Union[str, None]) -> "FAQPermission":
        if name is None:
            return cls.NONE

        return cls[name.upper()]


class FAQs(Cog):
    """Hold all commands for FAQs and explanations."""

    NAMES_SEPARATOR = "|"
    ALLOWED_FAQ_CHARS = re.compile(r"[0-9a-zA-Z-_() ',.!?:;\"=+]")

    def __init__(self, bot: Berzibot):
        self.bot = bot

    async def _verify_perms(
        self, ctx: BerziContext, required_level: FAQPermission
    ) -> bool:
        """Verify whether the context author meets the required perm level, if not, warn."""
        if await self.bot.is_owner(ctx.author):
            return True

        user_roles = list(map(lambda role: role.id, ctx.author.roles))
        async with self.bot.pool.acquire() as conn:
            user_perms = await conn.fetchval(
                "SELECT permissions FROM faqs_creators"
                " WHERE server=$1 AND role=ANY($2::bigint[])"
                " ORDER BY permissions DESC"
                " LIMIT 1;",
                ctx.guild.id,
                user_roles,
            )

        if FAQPermission.from_db_name(user_perms) < required_level:
            await ctx.send(fetch_line("insufficient permissions"))
            return False

        return True

    def _split_names(self, names: str) -> List[str]:
        result = list(filter(
            lambda txt: (
                txt and all(re.match(self.ALLOWED_FAQ_CHARS, char) for char in txt)
            ),
            map(lambda txt: txt.strip(), names.split(self.NAMES_SEPARATOR)),
        ))

        return result

    async def _get_similar_faqs(
        self, ctx: BerziContext, faq_name: str, limit: int = 3,
    ):
        async with self.bot.pool.acquire() as conn:
            similar = await conn.fetch(
                """
                SELECT n.text AS name FROM faqs_names n
                JOIN faqs_entries e ON n.faq_id = e.id
                WHERE e.server=$1 AND n.text % $2
                ORDER BY n.text % $2 DESC
                LIMIT $3;
                """,
                ctx.guild.id,
                faq_name,
                limit,
            )

        if not similar:
            message = fetch_line("fail find", f"a FAQ called `{faq_name}`")
        else:
            if len(similar) > 1:
                similar_options = ", ".join([f'"{n["name"]}"' for n in similar[:-1]])
                similar_options += f' or "{similar[-1]["name"]}"'
            else:
                similar_options = f'"{similar[0]["name"]}"'

            message = fetch_line("did you mean", similar_options)

        await ctx.channel.send(message)

    async def _get_faq_with_perms_or_fail(
        self, ctx: BerziContext, faq_name: str, perms: FAQPermission
    ) -> Optional[Record]:
        """Get a specified FAQ by name if the caller has adequate permissions."""
        async with self.bot.pool.acquire() as conn:
            faq = await conn.fetchrow(
                "SELECT *"
                " FROM faqs_entries e"
                " JOIN faqs_names n ON n.faq_id=e.id"
                " WHERE lower(n.text)=$1 AND e.server=$2",
                faq_name.strip().lower(),
                ctx.guild.id,
            )

        if not faq:
            await self._get_similar_faqs(ctx, faq_name)
            return None

        if not (
            faq["created_by"] == ctx.author.id or await self._verify_perms(ctx, perms)
        ):
            return None

        return faq

    @berzi_command(name="explain", aliases=["ex", "eli5", "about"])
    async def summon_faq(self, ctx: BerziContext, *, name: str):
        """Display the FAQ with the given name."""
        name = name.lower()

        if name in easter_eggs:
            await ctx.send(fetch_easter_egg(f"{name}"))
            return

        guild_id = ctx.guild.id

        async with self.bot.pool.acquire() as conn:
            entry = await conn.fetchrow(
                "SELECT e.content AS content,"
                " n.faq_id AS faq_id,"
                " n.text AS name,"
                " e.created_by AS author"
                " FROM faqs_entries e"
                " JOIN faqs_names n ON n.faq_id = e.id"
                " WHERE lower(n.text)=$1"
                " AND e.is_draft IS false"
                " AND e.server=$2"
                " LIMIT 1;",
                name,
                guild_id,
            )

        if not entry:
            await self._get_similar_faqs(ctx, name)
            await ctx.channel.send("I'll note this one down as a suggestion. :)")

            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    "INSERT INTO faqs_suggestions (server, name)"
                    " VALUES ($1, $2)"
                    " ON CONFLICT ON CONSTRAINT faqs_suggestions_name_server DO"
                    " UPDATE SET times=faqs_suggestions.times+1;",
                    guild_id,
                    name,
                )
            return

        # Record summon for stats.
        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                "INSERT INTO faqs_summons (faq_id, user_id) VALUES ($1, $2)",
                entry["faq_id"],
                ctx.author.id,
            )

        author = self.bot.get_user(entry["author"])

        # Split content into pages to avoid issues with embed limits
        pages = paginate_string(entry["content"], 1024, separator="\n")

        fields = [(None, p) for p in pages]

        paginator = EmbedPaginator(
            ctx,
            fields,
            page_size=2,  # Two fields per page, for a maximum of 2048 chars of content
            title=entry["name"],
            footer=(
                f"FAQ courtesy of {author.display_name}#{author.discriminator}. "
                f"Call `faq list` to see available FAQs.",
                None,
            ),
        )
        await paginator.paginate()

    @berzi_group(name="faq", invoke_without_command=True)
    async def faq(self, ctx: BerziContext, *, entry: str = None):
        """Commands for FAQ management and shorthand to summon FAQs."""
        if ctx.invoked_subcommand is None:
            if entry is None:
                await ctx.send(fetch_line("invalid subcommand", "faq"))
                await ctx.send("To read a FAQ, do `faq [name]` or `faq read [name]`.")
            else:
                await ctx.invoke(self.summon_faq, name=entry)

    @faq.berzi_command(name="read", aliases=["summon", "invoke", "show"])
    async def faq_read(self, ctx: BerziContext, *, name: str):
        """Alias for `explain [faq_name]`."""
        await ctx.invoke(self.summon_faq, name=name)

    @faq.berzi_command(name="perms")
    async def perms(
        self, ctx: BerziContext, perm_level: str = None, *, role: discord.Role = None,
    ):
        """Show, add or modify permissions for a role.
        If `perm_level` is "none", remove all permissions.
        If neither argument is given, show the current permissions.
        """

        if not await self._verify_perms(ctx, FAQPermission.PERMISSIONS):
            raise MissingPermissions([FAQPermission.PERMISSIONS.to_db_name()])

        server = ctx.guild
        server_id = server.id

        if perm_level is None and role is None:
            async with self.bot.pool.acquire() as conn:
                current_perms = await conn.fetch(
                    "SELECT role, permissions FROM faqs_creators WHERE server=$1;",
                    server_id,
                )

            paginator = EmbedPaginator(
                ctx,
                [
                    (server.get_role(perm["role"]).name, perm["permissions"])
                    for perm in current_perms
                ],
                page_size=5,
                title=f"Permissions for {server.name}",
            )
            await paginator.paginate()

            return

        try:
            level = FAQPermission.from_db_name(perm_level)
        except KeyError:
            await ctx.send(f'There\'s no permission level called "{perm_level}".')
            return

        if level is FAQPermission.NONE:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    "DELETE FROM faqs_creators WHERE server=$1 AND role=$2;",
                    server_id,
                    role.id,
                )
        else:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    "INSERT INTO faqs_creators (server, role, permissions)"
                    " VALUES ($1, $2, $3)"
                    " ON CONFLICT (role) DO"
                    " UPDATE SET permissions=EXCLUDED.permissions;",
                    server_id,
                    role.id,
                    level.to_db_name(),
                )

        await ctx.send(fetch_line("successful"))

    @faq.berzi_command(name="info")
    async def info(self, ctx: BerziContext, *, faq_name: str):
        """Show information on one FAQ."""
        guild = ctx.guild

        async with self.bot.pool.acquire() as conn:
            faq = await conn.fetchrow(
                "WITH temp AS (SELECT faq_id FROM faqs_names"
                " WHERE lower(text)=$1 AND server=$2)"
                " SELECT e.created_at AS created,"
                " e.created_by AS author,"
                " array_agg(n.text) AS names,"
                " (SELECT COUNT(*) FROM faqs_summons"
                " WHERE faq_id=temp.faq_id) AS summons"
                " FROM faqs_names n, temp"
                " JOIN faqs_entries e ON e.id=temp.faq_id"
                " WHERE n.faq_id=temp.faq_id AND e.server=$2"
                " GROUP BY e.id, temp.faq_id;",
                faq_name.lower(),
                guild.id,
            )

        if not faq:
            await self._get_similar_faqs(ctx, faq_name)
            return

        summons = faq["summons"]
        names = set()
        for name in faq["names"]:
            if name.lower() == faq_name.lower():
                faq_name = name
                continue

            names.add(name)

        output = (
            discord.Embed(
                author=self.bot.user,
                colour=self.bot.user.colour,
                title=f"**{faq_name}**",
                description=f"a.k.a.: {', '.join(names)}",
            )
            .add_field(
                name="Requested:",
                value=f"{summons} time{'' if summons == 1 else 's'}",
                inline=False,
            )
            .add_field(
                name="Written by:", value=guild.get_member(faq["author"]), inline=False,
            )
            .add_field(name="Written at:", value=faq["created"], inline=False,)
        )

        await ctx.send(embed=output)

    @faq.berzi_command(name="list", aliases=["all"])
    async def list(self, ctx: BerziContext):
        """List all available FAQs."""
        async with self.bot.pool.acquire() as conn:
            faqs = await conn.fetch(
                "SELECT array_agg(n.text) AS names"
                " FROM faqs_entries e"
                " JOIN faqs_names n ON e.id=n.faq_id"
                " WHERE e.server=$1"
                " GROUP BY e.id;",
                ctx.guild.id,
            )

        if not faqs:
            await ctx.send(fetch_line("fail find", "any FAQ"))
            return

        # Sort in alphabetical order by the main name
        faqs = sorted(faqs, key=lambda f: f["names"][0])

        paginator = EmbedPaginator(
            ctx,
            [
                (
                    f"**{row['names'][0]}**",
                    f"*a.k.a: {'/'.join(row['names'][1:])}*"
                    if row["names"][1:]
                    else None,
                )
                for row in faqs
            ],
            page_size=10,
            title="Available FAQs",
            description="Read a FAQ by pinging me with `explain [name]`.",
        )
        await paginator.paginate()

    @faq.berzi_command(name="suggestions", aliases=["ideas", "requests"])
    async def suggestions(self, ctx: BerziContext):
        """Show suggestions for FAQs (or aliases) that have been requested."""
        if not await self._verify_perms(ctx, FAQPermission.ALIASES):
            return

        def format_suggestion(sugg) -> str:
            return (
                f"(requested {sugg['times']} time"
                f"{'' if sugg['times'] == 1 else 's'})"
            )

        limit_suggestions = 25

        async with self.bot.pool.acquire() as conn:
            suggestions = await conn.fetch(
                f"SELECT name, times FROM faqs_suggestions"
                f" WHERE server=$1"
                f" ORDER BY times DESC, random()"
                f" LIMIT {limit_suggestions};",
                ctx.guild.id,
            )

        if not suggestions:
            message = "There are no suggestions at the moment."
        elif len(suggestions) == 1:
            message = f"The only suggestion is {format_suggestion(suggestions[0])}."
        else:
            suggestions_string = "\n".join(
                [
                    f"{suggestion['name']} {format_suggestion(suggestion)}"
                    for suggestion in suggestions
                ]
            )
            message = (
                f"Here are the {len(suggestions)} most frequent suggestions:\n"
                f"{suggestions_string}"
            )

        await ctx.send(message)

    @faq.berzi_group(name="new")
    async def new(self, ctx: BerziContext):
        """Commands to enter new FAQ material such as entries or alieases."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "new"))

    async def _add_names(
        self,
        faq_id: int,
        guild_id: int,
        names: List[str],
        existing_connection: asyncpg.pool.PoolAcquireContext = None,
    ):
        if not existing_connection:
            conn = await self.bot.pool.acquire()
        else:
            conn = existing_connection

        try:
            await conn.executemany(
                "INSERT INTO faqs_names (faq_id, text, server) VALUES ($1, $2, $3);",
                [(faq_id, name, guild_id) for name in names],
            )
        except UniqueViolationError:
            raise
        finally:
            await conn.executemany(
                "DELETE FROM faqs_suggestions WHERE server=$2 AND name=$1;",
                [(name, guild_id) for name in names],
            )

            if not existing_connection:
                await conn.close()

    def _get_duplicate_name(self, exc: UniqueViolationError) -> str:
        return f"\"{exc.detail.partition('=')[2].partition(',')[0][1:]}\""

    @new.berzi_command(name="name", aliases=["alias", "aliases", "names"])
    async def new_name(self, ctx: BerziContext, old_name: str, *, new_names: str):
        """Assign one or more new names to an existing FAQ. Separate names with |."""
        old = await self._get_faq_with_perms_or_fail(
            ctx, old_name, FAQPermission.ALIASES
        )

        if not old:
            return

        names = self._split_names(new_names)

        if not names:
            await ctx.send(fetch_line("invalid entry"))
            return

        try:
            await self._add_names(old["faq_id"], ctx.guild.id, names)
        except UniqueViolationError as e:
            await ctx.send(fetch_line("already exists", self._get_duplicate_name(e)))
            return

        await ctx.send(fetch_line("successful"))

    @new.berzi_command(name="faq", aliases=["entry"])
    async def new_faq(self, ctx: BerziContext, *, content: str):
        guild_id = ctx.guild.id

        if not await self._verify_perms(ctx, FAQPermission.CREATE):
            raise MissingPermissions([FAQPermission.CREATE.to_db_name()])

        if not content:
            await ctx.send("You're missing the content m8")
            return

        content = content.strip()

        # Require a name for the FAQ
        async with ctx.ask(
            f"Give me at least one name for this FAQ "
            f"(separate multiple names with `{self.NAMES_SEPARATOR}`).",
            preserve_case=True,
        ) as raw_names:
            if not raw_names:
                await ctx.send(fetch_line("cancelled"))
                return

            names = self._split_names(raw_names)

        if not names:
            await ctx.send(fetch_line("invalid entry"))
            return

        async with self.bot.pool.acquire() as conn:
            try:
                new_faq_id = await conn.fetchval(
                    """
INSERT INTO faqs_entries (server, created_by, content)
VALUES ($1, $2, $3)
RETURNING id;
                    """,
                    guild_id,
                    ctx.author.id,
                    content,
                )
            except UniqueViolationError:
                await ctx.send("I've got a FAQ with that exact content already.")
                return

            try:
                await self._add_names(new_faq_id, guild_id, names, conn)
            except UniqueViolationError as e:
                await ctx.send(fetch_line("already exists", self._get_duplicate_name(e)))

                # Delete orphaned faq
                await conn.execute(
                    """
DELETE FROM faqs_entries WHERE id=$1;
                    """,
                    new_faq_id,
                )
                return

        await ctx.send(fetch_line("successful"))

    @faq.berzi_group(name="edit", invoke_without_command=True)
    async def edit(self, ctx: BerziContext, *, entry: str = None):
        """Commands to edit FAQ material and shorthand to edit FAQ entries."""
        if ctx.invoked_subcommand is None:
            if entry is None:
                await ctx.send(fetch_line("invalid subcommand", "edit"))
                await ctx.send("To read a FAQ, do `faq [name]` or `faq read [name]`.")
            else:
                await ctx.invoke(self.edit_faq, faq_name=entry)

    @edit.berzi_command(name="faq", aliases=["entry"])
    async def edit_faq(self, ctx: BerziContext, *, faq_name: str):
        """Edit the content of an existing FAQ."""
        faq = await self._get_faq_with_perms_or_fail(
            ctx, faq_name, FAQPermission.MODIFY
        )
        if not faq:
            return

        # Send old content for convenience
        # Segment content to fit messages.
        old_content = faq["content"]
        old_content_parts = []
        while old_content:
            old_content_parts.append(old_content[:1500])
            old_content = old_content[1500:]

        await ctx.send("Here's the old content if you need it:")
        for part in old_content_parts:
            await ctx.send(f"```markdown\n" f"{part}```")

        done = False
        ask_msg = (
            f"Insert the new content (you have"
            f" {int(BerziContext.DEFAULT_ASK_TIMEOUT)} seconds)."
        )
        async with ctx.ask(ask_msg) as content:
            if content:
                async with ctx.ask("Are you sure?", boolean=True) as answer:
                    if answer:
                        async with self.bot.pool.acquire() as conn:
                            await conn.execute(
                                "UPDATE faqs_entries SET content=$2 WHERE id=$1;",
                                faq["faq_id"],
                                content,
                            )
                        done = True

        if done:
            await ctx.send(fetch_line("successful"))

    @faq.berzi_group(name="delete")
    async def delete(self, ctx: BerziContext):
        """Commands to remove FAQ material."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "delete"))
            await ctx.send(
                "If you want to delete a name or FAQ, tell me explicitly"
                " with `faq delete name [name]` or `faq delete entry [name]`"
            )

    @delete.berzi_command(name="faq", aliases=["entry", "content"])
    async def remove_content(self, ctx: BerziContext, *, faq_name: str):
        faq = await self._get_faq_with_perms_or_fail(
            ctx, faq_name, FAQPermission.DELETE
        )
        if not faq:
            return

        ask_msg = (
            f"Are you sure you want to delete"
            f" the FAQ {faq_name} and all of its aliases?"
        )
        async with ctx.ask(ask_msg, boolean=True) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        async with self.bot.pool.acquire() as conn:
            await conn.execute("DELETE FROM faqs_entries WHERE id=$1;", faq["faq_id"])

        await ctx.send(fetch_line("successful"))

    @delete.berzi_command(name="name", aliases=["alias"])
    async def remove_name(self, ctx: BerziContext, *, name: str):
        name = name.strip()

        faq = await self._get_faq_with_perms_or_fail(ctx, name, FAQPermission.ALIASES)
        if not faq:
            return

        # Count remaining names for the given FAQ
        async with self.bot.pool.acquire() as conn:
            names_amount = await conn.fetchval(
                "SELECT COUNT(*) FROM faqs_names WHERE faq_id=$1;", faq["faq_id"],
            )

        if names_amount == 1:
            await ctx.send(
                "This is the only name of this FAQ."
                " If you want to change its name, make a new one first with"
                " `faq new name [old name] [new name]`.\n"
                "If you want to delete the FAQ, do it explicitly with"
                " `faq delete entry [name]`."
            )
            return

        ask_msg = f"Are you sure you want to delete {name}?"
        async with ctx.ask(ask_msg, boolean=True) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                "DELETE FROM faqs_names WHERE lower(text)=$1 AND server=$2;",
                name.lower(),
                ctx.guild.id,
            )

        await ctx.send(fetch_line("successful"))

    @delete.berzi_command(name="suggestions")
    async def flush_suggestions(self, ctx: BerziContext):
        if not await self._verify_perms(ctx, FAQPermission.DELETE):
            return

        async with self.bot.pool.acquire() as conn:
            await conn.execute("DELETE FROM faqs_suggestions WHERE true")

        await ctx.send(fetch_line("successful"))
