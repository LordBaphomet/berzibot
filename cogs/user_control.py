from typing import Dict, Iterable, Literal, Optional, Tuple, Union, Set, List, Callable
import discord
from discord import Guild, Member, Message, RawReactionActionEvent, TextChannel
from discord.ext import tasks
from discord.ext.commands import Cog, has_permissions, UserInputError
import datetime
from berzibot import Berzibot
from components.berzicommand import berzi_command
from components.context import BerziContext as Context
from components.localisation import fetch_line, fetch_user_message
from components.log import logger
from components.utils import time_converter, time_difference


def setup(bot: Berzibot):
    bot.add_cog(UserControl(bot))


class UserControl(Cog):
    """Hold all basic functionality."""

    DELETE_LOG_MAX_AGE = 4_320
    """Maximum age of messages in the delete log, in minutes."""

    # Populated in on_ready
    available_roles: Dict[
        discord.Guild, Dict[Optional[discord.Role], Set[discord.Role]]
    ] = None
    """Mapping of guilds to a mapping of each role and set of roles that it can claim.
       Roles mapped to None be claimed by anyone.
       Effectively, roles mapped to itself will allow one-time removal of it.
    """

    # Populated in on_ready from aliased_roles
    role_aliases: Dict[discord.Guild, Dict[discord.Role, List[str]]] = None

    # Populated in on_ready
    aliased_roles: Dict[discord.Guild, Dict[str, discord.Role]] = None
    """Case-insensitive aliases of roles to expand role searches by name, sorted by guild."""

    def __init__(self, bot: Berzibot):
        self.bot = bot
        self.ban_log: Dict[discord.Guild, Optional[discord.TextChannel]] = {}
        """Mapping of guilds to a channel to post deleted messages into. A value of None
        counts as no configured channel.
        """

        self.reaction_roles: Dict[int, Dict[str, List[int]]] = {}
        """Mapping of guild ids to mappings of emoji names to role ids they correspond
        to.
        """

        self.reaction_messages: Dict[int, Tuple[int, int]] = {}
        """Mapping of guild ids to mappings of message id to which a reaction results
        in a role assignment. The message is represented as a tuple of channel id and
        message id.
        """

        self.non_removable_roles: Dict[int, Set[int]] = {}

        self.exclusive_role_groups: Dict[int, List[tuple]] = {}

        self.mute_roles: Dict[int, discord.Role] = {}

    def cog_unload(self):
        self.unban.cancel()
        self.unmute_loop.cancel()

    @Cog.listener()
    async def on_ready(self):
        self._populate_assignable_roles()
        await self._populate_reactable_roles()

        await self._populate_mute_roles()

        self.unmute_loop.start()

        self.unban.start()

    async def _populate_mute_roles(self):
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch(
                """
            SELECT guild_id, role_id
            FROM uc_mute_roles
            """
            )

        for guild_id, role_id in records:
            guild: discord.Guild = self.bot.get_guild(guild_id)
            if not (role_obj := guild.get_role(role_id)):
                logger.warn(f"Role {role_id} for guild {guild.name} not found.")
                continue

            role_obj: discord.Role
            self.mute_roles[guild_id] = role_obj

    async def _populate_reactable_roles(self):
        # Skip if already populated (on_ready calls this and may trigger more than once)
        if self.reaction_roles and self.reaction_messages:
            return

        async with self.bot.pool.acquire() as conn:
            reactables = await conn.fetch(
                """
SELECT rm.guild_id, rm.channel_id, rm.message_id, rr.emoji, rr.role_id
FROM roles_messages rm
JOIN roles_roles rr ON (rm.guild_id=rr.guild_id);
                """
            )

        # Populate reaction roles and messages
        for guild_id, channel_id, message_id, emoji, role_id in reactables:
            if guild_id not in self.reaction_roles:
                self.reaction_roles[guild_id] = {}

            if (emoji_name := emoji.strip()) not in self.reaction_roles[guild_id]:
                self.reaction_roles[guild_id][emoji_name] = []

            self.reaction_roles[guild_id][emoji_name].append(role_id)
            self.reaction_messages[guild_id] = (channel_id, message_id)

        # Populate non-removable roles
        async with self.bot.pool.acquire() as conn:
            non_removables = await conn.fetch(
                """
SELECT guild_id, role_id
FROM roles_non_removable;
                """
            )

        for guild_id, role_id in non_removables:
            if guild_id not in self.non_removable_roles:
                self.non_removable_roles[guild_id] = set()

            self.non_removable_roles[guild_id].add(role_id)

        # Populate exclusive group roles
        async with self.bot.pool.acquire() as conn:
            exclusive_groups = await conn.fetch(
                """
SELECT guild_id, role_1, role_2, role_3, role_4, role_5
FROM roles_exclusive_groups;
                """
            )

        for guild_id, role_1, role_2, role_3, role_4, role_5 in exclusive_groups:
            if guild_id not in self.exclusive_role_groups:
                self.exclusive_role_groups[guild_id] = []

            self.exclusive_role_groups[guild_id].append(
                (role_1, role_2, role_3, role_4, role_5)
            )

        # Set initial reactions
        for guild_id_, message_data in self.reaction_messages.items():
            guild: Guild = self.bot.get_guild(guild_id_)

            message: Message = await guild.get_channel(message_data[0]).fetch_message(
                message_data[1]
            )

            await message.clear_reactions()

            for emoji in self.reaction_roles[guild_id_].keys():
                # Get emoji object if custom emoji else keep the name
                emoji = next((e for e in guild.emojis if e.name == emoji), emoji,)

                await message.add_reaction(emoji)

    def _populate_assignable_roles(self):
        italian_learning = self.bot.get_guild(240436732305211392)
        it_learning_lesson_host = italian_learning.get_role(666746845833330701)
        it_learning_learning = italian_learning.get_role(258322430966235136)

        sicilian_learning = self.bot.get_guild(496761859462922240)
        scn_learning_english = sicilian_learning.get_role(496781276788031488)
        scn_learning_italian = sicilian_learning.get_role(496781358946058253)

        self.available_roles = {
            italian_learning: {
                None: {
                    it_learning_learning,  # Learning Italian
                    italian_learning.get_role(585749741816774657),  # Voicechatter
                    italian_learning.get_role(255420724733411329),  # Tutor
                    italian_learning.get_role(240438200407949312),  # Level A
                    italian_learning.get_role(240438325150875649),  # Level B
                    italian_learning.get_role(240438376040366080),  # Level C
                },
                it_learning_lesson_host: {it_learning_lesson_host},
            },
            sicilian_learning: {
                None: {
                    sicilian_learning.get_role(496773378368339978),  # Level A
                    sicilian_learning.get_role(496773498526498816),  # Level B
                    sicilian_learning.get_role(496773572749033476),  # Level C
                    scn_learning_english,  # English speaker
                    scn_learning_italian,  # Italian speaker
                },
            },
        }

        self.aliased_roles = {
            italian_learning: {"learning": it_learning_learning,},
            sicilian_learning: {
                "english": scn_learning_english,
                "italian": scn_learning_italian,
            },
        }

        self.role_aliases = {}
        for guild in self.aliased_roles:
            self.role_aliases[guild] = {}
            for alias in self.aliased_roles[guild]:
                try:
                    self.role_aliases[guild][self.aliased_roles[guild][alias]].append(
                        alias
                    )
                except KeyError:
                    self.role_aliases[guild][self.aliased_roles[guild][alias]] = [alias]

    async def _reaction_role_assignment(self, payload: RawReactionActionEvent):
        # TODO roles with requirements (must have/must not have)
        guild_id = payload.guild_id

        if (
            payload.event_type != "REACTION_ADD"
            or payload.member.bot
            or payload.message_id != self.reaction_messages.get(guild_id, (0, 0))[1]
        ):
            return

        assignable_roles = self.reaction_roles.get(guild_id, {})
        guild: Guild = self.bot.get_guild(guild_id)
        channel: TextChannel = self.bot.get_channel(payload.channel_id)
        action: Optional[Literal["added", "removed"]] = None
        member: Member = payload.member

        if payload.emoji:
            roles = assignable_roles.get(payload.emoji.name, [])

            for role_id in roles:
                role = guild.get_role(role_id)

                if role is None:
                    continue

                # User has role: remove
                if role in member.roles:
                    if role_id not in self.non_removable_roles.get(guild_id, []):
                        await member.remove_roles(role, atomic=True)
                        action = "removed"

                        # Special case: give Learning Italian if removing Concentrato
                        # noinspection PyBroadException
                        try:
                            if role == guild.get_role(866833210386284554):
                                await member.add_roles(
                                    guild.get_role(258322430966235136), atomic=True
                                )
                        except Exception:
                            pass

                # User does not have role: add it
                else:
                    # Search for role in exclusive groups, remove any incompatibility
                    remove_roles = set()
                    for group in self.exclusive_role_groups.get(guild_id, []):
                        if role_id in group:
                            remove_roles |= set(group)

                    remove_roles -= {None}
                    remove_roles = [
                        guild.get_role(role_id_) for role_id_ in remove_roles
                    ]

                    if remove_roles:
                        await member.remove_roles(*remove_roles, atomic=True)

                    await member.add_roles(role, atomic=True)
                    action = "added"

        if action:
            try:
                await member.send(fetch_line("role action", fetch_line(f"role {action}")))
            except discord.errors.Forbidden:
                pass

        message = await channel.fetch_message(payload.message_id)
        await message.remove_reaction(payload.emoji, payload.member)

    @Cog.listener()
    async def on_raw_reaction_add(self, payload: RawReactionActionEvent):
        await self._reaction_role_assignment(payload)

    async def _unmute(self, to_unmute: Iterable[Tuple[int, int]]):
        for guild_id, user_id in to_unmute:
            guild: discord.Guild = self.bot.get_guild(guild_id)

            # Unmark as muted
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
                    UPDATE uc_mutes
                    SET is_muted=false
                    WHERE user_id=$1
                    AND guild_id=$2
                    AND is_muted=true
                    """,
                    user_id,
                    guild_id,
                )

            if not (member := guild.get_member(user_id)):
                continue

            member: discord.Member

            # Remove mute role
            role = self.mute_roles[guild_id]
            await member.remove_roles(role, reason="Served mute time.", atomic=True)

            # Reassign previous roles
            async with self.bot.pool.acquire() as conn:
                to_reassign: str = await conn.fetchval(
                    """
                    SELECT roles
                    FROM uc_muted_users_roles
                    WHERE guild_id=$1 AND user_id=$2
                    """,
                    guild_id,
                    user_id,
                )

                await conn.execute(
                    """
                    DELETE FROM uc_muted_users_roles
                    WHERE guild_id=$1 AND user_id=$2
                    """,
                    guild_id,
                    user_id,
                )

            if to_reassign:
                roles = list(
                    map(
                        lambda role_id: guild.get_role(int(role_id)),
                        filter(lambda role_: bool(role_), to_reassign.split(",")),
                    ),
                )

                await member.add_roles(
                    *roles,
                    reason="Reassigning roles from before the mute.",
                    atomic=True,
                )

            logger.info(f"Unmuted {member.display_name} from guild {guild.name}.")

    @tasks.loop(minutes=1.0)
    async def unmute_loop(self):
        async with self.bot.pool.acquire() as conn:
            unmute = await conn.fetch(
                """
            SELECT guild_id, user_id
            FROM uc_mutes
            WHERE is_muted=true
            AND unmute_at<=NOW()
            """
            )

        await self._unmute(unmute)

    @tasks.loop(minutes=30.0)
    async def clean_log(self):
        """Purge older messages from the delete log."""
        limit = datetime.datetime.now() - datetime.timedelta(
            minutes=self.DELETE_LOG_MAX_AGE
        )

        for guild in self.bot.guilds:
            log_channel = await self.get_log_channel(guild)
            await log_channel.purge(before=limit, bulk=True)

    @tasks.loop(minutes=5.0)
    async def unban(self):
        async with self.bot.pool.acquire() as conn:
            unban_list = await conn.fetch(
                "SELECT server, banned_user"
                " FROM uc_bans"
                " WHERE ban_end IS NOT NULL AND ban_end <= now()"
                " ORDER BY server;"
            )

            for server_id, user_id in unban_list:
                guild = self.bot.get_guild(server_id)
                user = self.bot.get_user(user_id)

                await conn.execute("DELETE FROM uc_bans WHERE banned_user=$1", user_id)

                if not guild or not user:
                    continue

                try:
                    await guild.unban(user, reason="Ban time elapsed.")
                except discord.NotFound:
                    continue

                logger.info(f"{user} has been automatically unbanned from {guild}.")

    @unban.before_loop
    async def pre_unban(self):
        await self.bot.wait_until_ready()

    @staticmethod
    async def _log_deleted_message(
        message: discord.Message, log_channel: discord.TextChannel
    ):
        if log_channel is None:
            return

        author = message.author

        embed = discord.Embed()
        embed.title = f"Message deleted in {message.channel.name}"
        embed.colour = discord.Colour(0xAA0000)
        embed.set_author(
            name=f"{author.name}#{author.discriminator}", icon_url=author.avatar_url,
        )

        # Split content to circumvent embed field size limit.
        content = message.clean_content or ""
        if not content:
            embed.add_field(name="\N{ZERO WIDTH SPACE}", value="\N{ZERO WIDTH SPACE}")
        else:
            chunk_size = 1000
            for i in range(0, len(content), chunk_size):
                embed.add_field(
                    name="\N{ZERO WIDTH SPACE}", value=content[i : i + chunk_size]
                )

        embed.add_field(
            name="Created at:", value=message.created_at.isoformat(), inline=False
        )

        if date_edited := message.edited_at:
            embed.add_field(
                name="Edited at:", value=date_edited.isoformat(), inline=False
            )

        files = []
        if attachments := message.attachments:
            failed = 0
            try:
                for attachment in attachments:
                    as_file = await attachment.to_file()
                    files.append(as_file)
            except (discord.HTTPException, discord.Forbidden, discord.NotFound):
                failed += 1

            if failed:
                embed.add_field(
                    name=f"{failed} out of {len(attachments)} could not be loaded.",
                    value="\N{ZERO WIDTH SPACE}",
                    inline=False,
                )

        await log_channel.send(embed=embed, files=files)

        if embed_list := message.embeds:
            await log_channel.send("It had the following embeds:")
            for embd in embed_list:
                await log_channel.send(embed=embd)

    async def get_log_channel(
        self, guild: discord.Guild
    ) -> Optional[discord.TextChannel]:
        """Return the delete log channel for the guild or None, and set the cache."""
        try:
            if (log_channel := self.ban_log.get(guild, None)) is None:
                return None
        except KeyError:
            async with self.bot.pool.acquire() as conn:
                log_channel_id = await conn.fetchval(
                    "SELECT channel"
                    " FROM uc_delete_logs"
                    " WHERE server=$1"
                    " LIMIT 1;",
                    guild.id,
                )

            if log_channel_id is None:
                self.ban_log[guild] = None
                return None

            log_channel = guild.get_channel(log_channel_id)
            self.ban_log[guild] = log_channel

        return log_channel

    @Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        if message.author.bot:
            return

        # noinspection PyTypeChecker
        guild: discord.Guild = message.guild

        log_channel = await self.get_log_channel(guild)

        await self._log_deleted_message(message, log_channel)

    @has_permissions(manage_channels=True)
    @berzi_command(name="deletelog", meta={"mod_only": True})
    async def set_delete_log(
        self, ctx: Context, *, channel: Union[discord.TextChannel, str] = "here"
    ):
        """Set a channel to store deleted messages. Only one channel per server is supported. Enter `none` to disable delete logs."""
        if isinstance(channel, str):
            if (channel := channel.lower()) == "here":
                channel = ctx.channel
            elif channel != "none":
                raise UserInputError

        guild = ctx.guild
        guild_id = guild.id

        async with self.bot.pool.acquire() as conn:
            if channel == "none":
                await conn.execute(
                    "DELETE FROM uc_delete_logs WHERE server=$1;", guild_id
                )
                self.ban_log[guild] = None
            else:
                await conn.execute(
                    "INSERT INTO uc_delete_logs (server, channel)"
                    " VALUES ($1, $2)"
                    " ON CONFLICT (server) DO UPDATE"
                    " SET channel=$2;",
                    guild_id,
                    channel.id,
                )
                self.ban_log[guild] = channel

        await ctx.send(fetch_line("successful"))
        logger.info(f"{ctx.author} set a delete log for {guild=} in {channel=}.")

    @has_permissions(ban_members=True)
    @berzi_command(name="ban", meta={"mod_only": True})
    async def ban(self, ctx: Context, user: discord.User, *, time: time_converter):
        """Ban a user for a certain amount of time, after which it will be automatically unbanned."""
        guild = ctx.guild
        await guild.ban(user)

        for_time = ""
        if time:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    "INSERT INTO uc_bans"
                    " (server, banning_user, banned_user, ban_end)"
                    " VALUES ($1, $2, $3, $4);",
                    ctx.guild.id,
                    ctx.author.id,
                    user.id,
                    time,
                )
            time_diff = time_difference(to=time)
            for_time = (
                f" for " + (", ".join(time_diff[:-1]) + f" and {time_diff[-1]}")
                if len(time_diff) > 1
                else time_diff[-1]
            )

        async with ctx.ask(
            "Done. Should I also remove their messages from today?", boolean=True
        ) as answer:
            if answer:
                await guild.unban(user)
                await guild.ban(user, delete_message_days=1)

        reason = ""
        async with ctx.ask("Should I give them a reason?") as answer:
            if answer:
                await guild.unban(user)
                await guild.ban(user, reason=answer)
                reason = " The following reason was given:\n> "
                reason += "\n> ".join(answer.split("\n"))
            else:
                answer = ""

        dm = user.dm_channel or await user.create_dm()
        await dm.send(f"You have been banned from {guild.name}{for_time}.{reason}")

        logger.warn(f"{ctx.author} banned {user}{for_time}. Reason, if given: {answer}")

    def _roles_for_user(
        self,
        guild: discord.Guild,
        member: discord.Member,
        filter_func: Callable = lambda x: x,
    ) -> set:
        guild_roles = self.available_roles[guild]

        available_for_user = set()
        available_for_user |= guild_roles[None]
        for role in member.roles[1:]:  # First role is guaranteed to be @everyone
            available_for_user |= guild_roles[role]

        return set(filter(filter_func, available_for_user))

    def _role_names(
        self, guild: discord.Guild, roles: Set[discord.Role]
    ) -> Dict[discord.Role, List[str]]:
        roles_names = {role: [role.name] for role in roles}
        for role in roles_names.keys():
            roles_names[role].extend(self.role_aliases[guild][role])

        return roles_names

    def _roles_list(self, guild: discord.Guild, roles: Set[discord.Role]) -> str:
        roles_names = self._role_names(guild, roles)

        return "\n".join("🔸 " + ", ".join(names) for role, names in roles_names.items())

    # TODO Automatic time escalation if duration not supplied
    @has_permissions(manage_roles=True)
    @berzi_command(name="mute", meta={"mod_only": True})
    async def mute(self, ctx: Context, user: discord.User, *, duration: time_converter):
        """Mute a user, specifying a duration after which they will be automatically unmuted."""
        user_id = user.id
        guild = ctx.guild
        guild_id = guild.id
        member: discord.Member = guild.get_member(user_id)

        if guild_id not in self.mute_roles:
            await ctx.send("No mute role is configured for this server.")
            return

        if (removable_roles := member.roles[1:]) :
            await member.remove_roles(*removable_roles)

        await member.add_roles(
            self.mute_roles[guild_id], reason=f"Muted by {ctx.author.display_name}",
        )

        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                """
                INSERT INTO uc_mutes (guild_id, user_id, unmute_at, is_muted)
                VALUES ($1, $2, $3, true)
                """,
                guild_id,
                user_id,
                duration,
            )

            if removable_roles:
                # Save removed roles to put them back later
                roles_as_string = ",".join(
                    map(lambda role: str(role.id), removable_roles)
                )

                await conn.execute(
                    """
                    INSERT INTO uc_muted_users_roles (guild_id, user_id, roles)
                    VALUES ($1, $2, $3)
                    """,
                    guild_id,
                    user_id,
                    roles_as_string,
                )

        await ctx.send(fetch_line("successful"))

        logger.info(
            f"{user.name} has been muted from {guild.name}"
            f" by {ctx.author.display_name}"
            f"{f' until {duration}' if duration else ''}."
        )

    @has_permissions(manage_roles=True)
    @berzi_command(name="unmute", meta={"mod_only": True})
    async def unmute(self, ctx: Context, user: discord.User):
        """Unmute a user, if currently muted."""
        user_id = user.id
        guild = ctx.guild
        guild_id = guild.id

        await self._unmute(((guild_id, user_id),))

        await ctx.send(fetch_line("successful"))
        await ctx.send(fetch_line("might take time"))

        logger.info(
            f"{user.name} has been unmuted from {guild.name}"
            f" by {ctx.author.display_name} (if they were muted)."
        )

    @has_permissions(mute_members=True)
    @berzi_command(name="warn")
    async def warn(self, ctx: Context, user: discord.User, *, duration: time_converter):
        """Like `mute`, but also notifies the user of the mute."""
        await self.mute(ctx, user, duration=duration)
        await user.send(fetch_user_message("muted", ctx.guild.name))

    @berzi_command(name="roles", aliases=["listroles"])
    async def list_roles(self, ctx: Context):
        """List the roles I can give you."""
        if not self.bot.is_ready():
            await ctx.send(fetch_line("not ready"))
            return

        roles = self._roles_list(ctx.guild, ctx.author.roles)

        await ctx.send("Here are the roles I can give you:\n" + roles)

    @berzi_command(name="gimme", aliases=["giverole"])
    async def give_role(self, ctx: Context, *, role_name: Union[discord.Role, str]):
        """Get the selected role for yourself. Pick the role by name or ID."""
        if not self.bot.is_ready():
            await ctx.send(fetch_line("not ready"))
            return

        guild = ctx.guild
        user = ctx.author

        # List roles that can be given to user, exclude those they already have.
        if not role_name:
            roles_for_user = self._roles_for_user(
                guild, user, lambda r: r not in user.roles,
            )
            roles = self._roles_list(guild, roles_for_user)
            await ctx.send("Here are the roles I can give you:\n" + roles)
            return

        if isinstance(role_name, str):
            role_name = role_name.lower().strip()
            try:
                role = self.aliased_roles[guild][role_name]
            except KeyError:
                await ctx.send(fetch_line("fail find", role_name))
                return
        else:
            role = role_name

        await user.add_roles([role], reason="Bot command", atomic=True)
        await ctx.send(fetch_line("successful"))

    @berzi_command(name="remove", aliases=["removerole"])
    async def remove_role(self, ctx: Context, *, role_name: Union[discord.Role, str]):
        """Remove the selected role from yourself. Pick the role by name or ID."""
        if not self.bot.is_ready():
            await ctx.send(fetch_line("not ready"))
            return

        guild = ctx.guild
        user = ctx.author

        # List roles that can be removed from user.
        if not role_name:
            roles_for_user = self._roles_for_user(
                guild, user, lambda r: r in user.roles,
            )
            roles = self._roles_list(ctx.guild, roles_for_user)
            await ctx.send("Here are the roles I can remove from you:\n" + roles)
            return

        if isinstance(role_name, str):
            role_name = role_name.lower().strip()
            try:
                role = self.aliased_roles[guild][role_name]
            except KeyError:
                await ctx.send(fetch_line("fail find", role_name))
                return
        else:
            role = role_name

        await user.remove_roles([role], reason="Bot command", atomic=True)
        await ctx.send(fetch_line("successful"))
