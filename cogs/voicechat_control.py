import discord
from typing import DefaultDict, List, Optional, Dict
from discord.ext.commands import Cog
from discord.utils import get
from berzibot import Berzibot
from dataclasses import dataclass
from collections import defaultdict
from components.berzicommand import berzi_command
from components.context import BerziContext


@dataclass
class TriggerChannel:
    guild: int
    channel: Optional[discord.VoiceChannel]
    category_id: Optional[int]
    name: str
    child_name: str
    size: int


def setup(bot: Berzibot):
    bot.add_cog(VoiceChatControl(bot))


class VoiceChatControl(Cog):
    """Hold all commands for controlling voicechat and channels."""

    def __init__(self, bot: Berzibot):
        self.bot = bot
        self.trigger_channels: Dict[int, TriggerChannel] = {}
        """Mapping of trigger channel IDs to TriggerChannel objects."""

        self.parents_children: DefaultDict[int, List[int]] = defaultdict(list)
        """Mapping of trigger channel IDs to list of temporary channel IDs derived from it."""

        self.children_parents: Dict[int, TriggerChannel] = {}
        """Mapping of temporary channel IDs to parent trigger channel."""

        self.ready = False

    @Cog.listener()
    async def on_ready(self):
        await self.collect_trigger_channels()

    @berzi_command("clean_temp_channels", meta={"mod_only": True})
    async def clean_up_channels(self, _: BerziContext):
        reason = "Cleaning up temporary channels."
        parents = set()
        for child_id, parent in self.children_parents:
            parents.add(parent)
            channel: discord.VoiceChannel = self.bot.get_channel(child_id)
            await channel.delete(reason=reason)

        for parent in parents:
            parent.channel.delete(reason=reason)

    async def collect_trigger_channels(self):
        async with self.bot.pool.acquire() as conn:
            channels = await conn.fetch(
                """
            SELECT guild_id,
            trigger_channel_name,
            new_channel_name,
            channel_size,
            category_id
            FROM voice_trigger_channels
            """
            )

        triggers = []
        for (
            guild_id,
            trigger_channel_name,
            new_channel_name,
            channel_size,
            category_id,
        ) in channels:
            triggers.append(
                TriggerChannel(
                    guild=guild_id,
                    category_id=category_id,
                    name=trigger_channel_name.strip(),
                    child_name=new_channel_name.strip(),
                    size=channel_size,
                    channel=None,
                )
            )

        # Make trigger channels
        for trigger in triggers:
            guild: discord.Guild = self.bot.get_guild(trigger.guild)
            category: discord.CategoryChannel = get(
                guild.categories, id=trigger.category_id
            )
            final_name = f"➕ {trigger.name}"

            # Search if channel already exists
            for channel in guild.voice_channels:
                if channel.name == final_name:
                    channel.edit(
                        user_limit=1,
                        category=category,
                        reason="Updating trigger channel.",
                    )
                    trigger.channel = channel
                    break
            else:
                trigger.channel = await guild.create_voice_channel(
                    name=final_name, category=category, user_limit=1,
                )

            self.trigger_channels.update({trigger.channel.id: trigger})

        self.ready = True

    @Cog.listener()
    async def on_voice_state_update(
        self,
        member: discord.Member,
        before: discord.VoiceState,
        after: discord.VoiceState,
    ):
        if member.bot or not self.ready:
            return

        if (entered := after.channel) is not None and before.channel != entered:
            await self.on_entered_channel(member, entered)

        if (exited := before.channel) is not None and after.channel != exited:
            await self.on_exited_channel(member, exited)

    async def on_entered_channel(
        self, member: discord.Member, channel: discord.VoiceChannel
    ):
        if channel.id in self.trigger_channels:
            await self.make_new_temporary_channel(member, channel)

        if channel.id in self.children_parents:
            ...  # TODO Check if now full -> hide

    async def make_new_temporary_channel(
        self, member: discord.Member, channel: discord.VoiceChannel
    ):
        # TODO also make text channel (private for users in there)
        parent = self.trigger_channels[channel.id]
        existing = self.parents_children[parent.channel.id]

        guild: discord.Guild = channel.guild
        category: discord.CategoryChannel = get(guild.categories, id=parent.category_id)
        new_channel = await guild.create_voice_channel(
            name=parent.child_name, category=category, user_limit=parent.size,
        )

        existing.append(new_channel.id)
        self.children_parents.update({new_channel.id: parent})

        await member.move_to(
            new_channel, reason=f"Created new temporary channel from {parent.name}",
        )

    async def on_exited_channel(self, _: discord.Member, channel: discord.VoiceChannel):
        if channel.id in self.children_parents:
            if len(channel.members) == 0:
                await channel.delete(reason="Temporary channel was empty.")

                parent = self.children_parents[channel.id]
                try:
                    self.parents_children[parent.channel.id].remove(channel.id)
                except ValueError:
                    pass

                del self.children_parents[channel.id]
