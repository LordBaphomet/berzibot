from copy import copy
from discord.ext.commands import Command, command, Group


class HasMeta:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._meta = copy(kwargs.get("meta", {}))
        if not isinstance(self._meta, dict):
            raise ValueError

    def get_meta(self, key: str):
        return self._meta.get(key, None)


class BerziCommand(HasMeta, Command):
    """Command class able to store arbitrary attributes in the meta property."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def berzi_command(name=None, cls=BerziCommand, **attrs):
    """Transforms a function into a :class:`BerziCommand`."""

    def decorator(func):
        if isinstance(func, Command):
            raise TypeError("Callback is already a command.")
        return cls(func, name=name, **attrs)

    return decorator


class BerziGroup(HasMeta, Group):
    """Group class able to store arbitrary attributes in the meta property."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def berzi_command(self, *args, **kwargs):
        """A shortcut decorator that invokes :func:`.berzi_command` and adds it to
        the internal command list via :meth:`~.GroupMixin.add_command`.
        """

        def decorator(func):
            kwargs.setdefault("parent", self)
            result = berzi_command(*args, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    def berzi_group(self, *args, **kwargs):
        """A shortcut decorator that invokes :func:`.berzi_group` and adds it to
        the internal command list via :meth:`~.GroupMixin.add_command`.
        """

        def decorator(func):
            kwargs.setdefault("parent", self)
            result = berzi_group(*args, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator


def berzi_group(name=None, **attrs):
    """Transforms a function into a :class:`BerziGroup`."""
    attrs.setdefault("cls", BerziGroup)
    return command(name=name, **attrs)
