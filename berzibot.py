import asyncio
import re
import sys
import traceback
from copy import copy
from typing import Optional
import aiohttp
import asyncpg
import discord
from discord.ext.commands import (
    Bot,
    CheckFailure,
    CommandInvokeError,
    NotOwner,
    UserInputError,
)
from components.context import BerziContext
from components.expiring_cache import ExpiringCache
from components.localisation import fetch_line, fetch_set
from components.log import logger
from config import TOKEN, RECENT_COMMAND_TIME
from secrets import (
    DB_TYPE,
    DB_USER,
    DB_PASSWORD,
    DB_HOST,
    DB_PORT,
    DB_NAME,
)

try:
    from secrets import DEBUG, TEST_GUILD
except ImportError:
    DEBUG = False
    TEST_GUILD = 0

DB_STRING = (
    f"{DB_TYPE}://"
    f"{DB_USER}{':' + DB_PASSWORD if DB_PASSWORD else ''}"
    f"@{DB_HOST}{':' + DB_PORT if DB_PORT else ''}"
    f"{'/' + DB_NAME if DB_NAME else ''}"
)


def _prefix_callable(bot, msg):
    user_id = bot.user.id
    base = [f"<@!{user_id}> ", f"<@{user_id}> "]
    if msg.guild is None:
        base.append("?")
    else:
        base.extend(bot.prefixes)
    return base


class Berzibot(Bot):
    default_activity = discord.Activity(
        type=discord.ActivityType.listening, name=fetch_line("listening to")
    )

    _sleeping = False
    _sending_errors = False

    async def _populate_db_pool(self):
        self.pool = await asyncpg.create_pool(DB_STRING)

    async def _populate_aiohttp_session(self):
        self.session = aiohttp.ClientSession()

    def __init__(self, command_prefix, **options):
        super().__init__(command_prefix, **options)

        self.pool: Optional[asyncpg.pool.Pool] = None
        self.loop.create_task(self._populate_db_pool())

        self.session: Optional[aiohttp.ClientSession] = None
        self.loop.create_task(self._populate_aiohttp_session())

        # Additional prefixes for the bot.
        self.prefixes = ["Berzibot, ", "Berzibot! ", "Berzibot? "]
        self.prefixes.extend(map(lambda s: s.lower(), self.prefixes.copy()))
        if DEBUG:
            self.prefixes.append("test, ")

        self.command_keywords = {
            "and": self._new_command_shortcut,
            "again with": self._command_with_new_args,
            "again": self._repeat_command,
            **{
                line.lower(): self._respond_to_praise
                for line in fetch_set("appreciation")
            },
        }

        keyword_regex = rf"^(?P<kw>{'|'.join(self.command_keywords.keys())})"
        self.keyword_matcher = re.compile(keyword_regex, flags=re.IGNORECASE)

        self._latest_commands = ExpiringCache(RECENT_COMMAND_TIME)
        """Dict of callers to a tuple of the invoked context and monotonic time of
        invocation.
        """

    @property
    def sleeping(self):
        return self._sleeping

    @property
    def sending_errors(self):
        return self._sending_errors

    async def set_sleeping(self, value: bool):
        if value is True:
            self._sleeping = True
            activity = discord.Game(fetch_line("playing dead"))
            await self.change_presence(
                activity=activity, status=discord.Status.idle, afk=True
            )
        elif value is False:
            self._sleeping = False
            await self.change_presence(
                activity=self.default_activity, status=discord.Status.online, afk=False
            )
        else:
            raise ValueError

    def set_sending_errors(self, value: bool):
        if isinstance(value, bool):
            self._sending_errors = value
        else:
            raise ValueError

    async def _new_command_shortcut(self, message: discord.Message, ctx: BerziContext):
        new_message = copy(message)
        new_message.content = ctx.prefix + message.content[4:]
        await self.process_commands(new_message)

    async def _command_with_new_args(self, message: discord.Message, ctx: BerziContext):
        # Use original message content to preserve command arg integrity
        # (case sensitive commands, etc).
        new_args = message.content[len("again with ") :]
        new_message = copy(message)

        # Hand command fragment down the context chain.
        new_content = f"{ctx.prefix}{ctx.command.qualified_name} {new_args}"
        new_message.content = new_content

        await self.process_commands(new_message)

    async def _repeat_command(self, _: discord.Message, ctx: BerziContext):
        new_message = copy(ctx.message)
        await self.process_commands(new_message)

    # noinspection PyMethodMayBeStatic
    async def _respond_to_praise(self, _: discord.Message, ctx: BerziContext):
        await ctx.send(fetch_line("respond to praise"))

    async def process_commands(self, message: discord.Message):
        # Remove excessive spaces to make faultily-formatted commands work.
        # Note: this disallows multiple spaces in argument contents.
        message.content = re.sub(r" +", " ", message.content)

        ctx = await self.get_context(message, cls=BerziContext)

        if not ctx.guild:
            return

        if ctx.command is None:
            if ctx.guild.me in message.mentions:
                await ctx.send(fetch_line("hello"))
            return

        if self.sleeping and not await self.is_owner(message.author):
            await ctx.send(fetch_line("sleeping"))
            return

        try:
            async with ctx.acquire():
                await self.invoke(ctx)
        except Exception:
            raise
        else:
            self._latest_commands[message.author.id] = ctx

    async def on_ready(self):
        await self.change_presence(
            activity=self.default_activity, status=discord.Status.online, afk=False
        )

    async def on_command_error(self, ctx, error):
        if isinstance(error, CommandInvokeError):
            err = error.original
            exc_info = (type(err), err, err.__traceback__)
            exc = "".join(traceback.format_exception(*exc_info))
            logger.error(exc)
            print(exc)

        if isinstance(error, NotOwner):
            await ctx.send(fetch_line("only owner"))
            return

        if isinstance(error, CheckFailure):
            await ctx.send(fetch_line("insufficient permissions"))
            return

        if isinstance(error, UserInputError):
            await ctx.send(fetch_line("user error"))
            return

        await ctx.send(fetch_line("fail"))

    async def on_error(self, event, *args, **kwargs):
        exception_info = sys.exc_info()
        if not isinstance(exception_info[0], asyncio.exceptions.TimeoutError):
            traceback.print_exc()
            logger.exception(exception_info)
            if self.sending_errors:
                owner = self.get_user(self.owner_id)
                async with owner.create_dm() as dm_channel:
                    await dm_channel.send(exception_info)

    async def on_message(self, message: discord.Message):
        if message.author.bot:
            return

        if DEBUG and message.guild.id != TEST_GUILD:
            return

        contents = message.content
        if contents.startswith(tuple(self.command_keywords)):
            if old_ctx := self._latest_commands.fetch(message.author.id):
                keyword = self.keyword_matcher.match(contents).group("kw").lower()
                if keyword:
                    func = self.command_keywords[keyword]
                    # noinspection PyArgumentList
                    await func(message, old_ctx)
                    return

        await self.process_commands(message)


intents = discord.Intents.default()
intents.members = True
intents.presences = True
berzibot = Berzibot(command_prefix=_prefix_callable, intents=intents)

if __name__ == "__main__":
    # Remove default help to override with custom one in basic_functionality
    berzibot.remove_command("help")

    berzibot.load_extension("cogs.basic_functionality")
    berzibot.load_extension("cogs.chat_control")
    berzibot.load_extension("cogs.user_control")
    berzibot.load_extension("cogs.thing_of_the_day")
    berzibot.load_extension("cogs.faqs")
    berzibot.load_extension("cogs.voicechat_control")
    if DEBUG:
        berzibot.load_extension("cogs.testing")

    berzibot.run(TOKEN)
