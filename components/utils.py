import asyncio
import datetime
import re
from typing import Awaitable, Callable, Dict, List, Optional, Sequence, Tuple, Union

import discord
from discord.embeds import EmptyEmbed

from components.context import BerziContext
from config import DEFAULT_TIMEOUT, PAGE_SIZE

capital = re.compile(r"(\w+?) *(?=[A-Z]|\b)")
EMOJI = {
    "q": "🇶",
    "w": "🇼",
    "e": "🇪",
    "r": "🇷",
    "t": "🇹",
    "y": "🇾",
    "u": "🇺",
    "i": "🇮",
    "o": "🇴",
    "p": "🇵",
    "a": "🇦",
    "s": "🇸",
    "d": "🇩",
    "f": "🇫",
    "g": "🇬",
    "h": "🇭",
    "j": "🇯",
    "k": "🇰",
    "l": "🇱",
    "z": "🇿",
    "x": "🇽",
    "c": "🇨",
    "v": "🇻",
    "b": "🇧",
    "n": "🇳",
    "m": "🇲",
    "1": "1️⃣",
    "2": "2️⃣",
    "3": "3️⃣",
    "4": "4️⃣",
    "5": "5️⃣",
    "6": "6️⃣",
    "7": "7️⃣",
    "8": "8️⃣",
    "9": "9️⃣",
    "0": "0️⃣",
}
NON_EMOJI = {v: k for k, v in EMOJI.items()}

TIME_UNIT_PATTERN = re.compile(r"(\d+)\s*?(\w+)")
BASIC_TIME_DESCRIPTORS = {
    "weeks": "weeks",
    "week": "weeks",
    "w": "weeks",
    "days": "days",
    "day": "days",
    "d": "days",
    "hours": "hours",
    "hour": "hours",
    "hrs": "hours",
    "hr": "hours",
    "h": "hours",
    "minutes": "minutes",
    "minute": "minutes",
    "mins": "minutes",
    "min": "minutes",
    "m": "minutes",
    "seconds": "seconds",
    "second": "seconds",
    "secs": "seconds",
    "sec": "seconds",
    "s": "seconds",
    "milliseconds": "milliseconds",
    "millisecond": "milliseconds",
    "ms": "milliseconds",
    "microseconds": "microseconds",
    "microsecond": "microseconds",
    "µs": "microseconds",
    "us": "microseconds",
}
"""Mapping of time descriptors to their name as a timedelta keyword argument."""

EXTRA_TIME_DESCRIPTORS = {
    "months": lambda v: ("weeks", v * 4),
    "years": lambda v: ("days", v * 365 - (v // 4)),
    "decades": lambda v: ("days", v * 365 * 10 - ((v * 10) // 4)),
}
"""Mapping of extra time descriptors requiring conversion.

The conversion callable must return the equivalent timedelta keyword argument and
the converted value.
"""

# Synonyms for extra descriptors:
EXTRA_TIME_DESCRIPTORS.update(
    {
        "mo": EXTRA_TIME_DESCRIPTORS["months"],
        "month": EXTRA_TIME_DESCRIPTORS["months"],
        "y": EXTRA_TIME_DESCRIPTORS["years"],
        "yr": EXTRA_TIME_DESCRIPTORS["years"],
        "yrs": EXTRA_TIME_DESCRIPTORS["years"],
    }
)


def to_emoji_case(string: str) -> str:
    """Convert letters (ascii) and numbers to emoji."""
    return "".join(EMOJI.get(ch.lower(), ch) for ch in string)


def from_emoji_case(string: str) -> str:
    """Convert emoji to letters and numbers."""
    return "".join(NON_EMOJI.get(ch, ch) for ch in string)


def to_snake_case(string: str) -> str:
    """Convert a string to snake_case."""
    return capital.sub(r"\1_", string).lower().rstrip("_")


def from_snake_case(string: str) -> str:
    """Convert a string from snake_case to lower case."""
    middle = string[1:-1].replace("_", " ")
    return f"{string[0]}{middle}{string[-1]}"


def time_parts_from_string(string: str) -> Dict[str, int]:
    """Convert a human-language string to the equivalent in time parts as seen in datetime.timedelta arguments."""
    all_basic_descriptors = set(BASIC_TIME_DESCRIPTORS.keys())
    all_extra_descriptors = set(EXTRA_TIME_DESCRIPTORS.keys())
    all_descriptors = all_basic_descriptors | all_extra_descriptors

    time_units: List[Tuple[int, str]] = []
    search_string = string.lower().strip()
    while search_string:
        match = re.match(TIME_UNIT_PATTERN, search_string.strip())
        if not match:
            break

        time_units.append((int(match[1]), match[2]))

        _, _, search_string = search_string.partition(match[0])

    parts = {}
    for unit in time_units:
        value = unit[0]
        descriptor = unit[1].lower()

        if descriptor not in all_descriptors:
            raise ValueError(f"{descriptor} is not a recognised time descriptor.")

        if descriptor in all_basic_descriptors:
            descriptor = BASIC_TIME_DESCRIPTORS[descriptor]
        else:
            descriptor, value = EXTRA_TIME_DESCRIPTORS[descriptor](value)

        current = parts[descriptor] if descriptor in parts else 0
        parts[descriptor] = current + value

    if not parts:
        raise ValueError(f"No fitting descriptor found for '{string}'")

    return parts


def time_converter(argument: str) -> datetime.datetime:
    """Convert a textual time such as 23:41 to a time object with hour and minute."""
    if (argument := argument.lower()) == "now":
        time = current_timeofday()
    else:
        try:
            time = datetime.datetime.strptime(argument, "%H:%M").replace(
                tzinfo=datetime.timezone.utc
            )
        except ValueError:
            time_parts = time_parts_from_string(argument)
            time = current_timeofday() + datetime.timedelta(**time_parts)

    return time


def current_timeofday() -> datetime.datetime:
    """Return the current time of day as utc."""
    return datetime.datetime.now().utcnow().replace(tzinfo=datetime.timezone.utc)


def time_difference(
    *, from_: Union[datetime.datetime, str] = "now", to: Union[datetime.datetime, str]
) -> Tuple[str]:
    """Return a tuple of the (rough) differences in time between the arguments by time unit."""
    if not isinstance(from_, datetime.datetime):
        from_ = time_converter(from_)
    if not isinstance(to, datetime.datetime):
        to = time_converter(to)

    if from_.tzinfo is None:
        from_ = from_.replace(tzinfo=datetime.timezone.utc)
    if to.tzinfo is None:
        to = to.replace(tzinfo=datetime.timezone.utc)

    if from_ == to:
        return ("None",)

    diff = max(from_, to) - min(from_, to)

    units = {
        "years": lambda td: td.days // 365,
        "months": lambda td: (td.days - (units["years"](td) * 365)) // 30,
        "weeks": lambda td: (
            td.days - (units["months"](td) * 30 + units["years"](td) * 365)
        )
        // 7,
        "days": lambda td: td.days
        - (
            units["weeks"](td) * 7 + units["months"](td) * 30 + units["years"](td) * 365
        ),
        "hours": lambda td: td.seconds // 60 // 60,
        "minutes": lambda td: (td.seconds - (units["hours"](td) * 60)) // 60,
        "seconds": lambda td: td.seconds
        - (units["minutes"](td) * 60 + units["hours"](td) * 60),
    }
    """Mapping of time part name to callables to extract them.
    
    Strictly in ascending order of precision. Each unit takes the remainder of the
    previous operations on the same property.
    """

    final_values = {unit: units[unit](diff) for unit in units.keys()}
    return tuple(
        f"{amount} {unit[:-1] if amount == 1 else unit}"
        for unit, amount in final_values.items()
        if amount
    )


class EmbedPaginator:
    # Thanks to nully for help with this.
    _forward_emoji = "▶️"
    _backward_emoji = "◀️"

    def __init__(
        self,
        ctx: BerziContext,
        entries: Sequence[Tuple[Optional[str], str]],
        *,
        page_size=PAGE_SIZE,
        timeout=DEFAULT_TIMEOUT,
        thumbnail: str = None,
        title: str = None,
        description: str = None,
        author: discord.User = None,
        footer: Tuple[Optional[str], Optional[str]] = None,
    ):
        """Create an embed with pagination. Show with `await paginator.paginate()`.
        
        :param ctx: the context to show the paginator in.
        :param entries: the fields of the embed as tuples of (title, content).
        :param page_size: amount of fields to show per page.
        :param timeout: time to wait for interaction by the user to flip page.
        :param thumbnail: thumbnail to show in the embed.
        :param title: title of the embed.
        :param description: description of the embed.
        :param author: author to show on the embed.
        :param footer: footer of the embed, as a tuple of (text, icon), both optional.
        """

        self._bot = ctx.bot
        self._message = ctx.message
        self._channel = ctx.channel
        self._author = ctx.author
        self._embed = discord.Embed(colour=self._author.colour)

        if footer:
            self._embed.set_footer(
                text=footer[0] or EmptyEmbed, icon_url=footer[1] or EmptyEmbed,
            )

        if thumbnail:
            self._embed.set_thumbnail(url=thumbnail)

        if title:
            self._embed.title = title

        if description:
            self._embed.description = description

        if author:
            self._embed.set_author(
                name=self._author.name, icon_url=self._author.avatar_url
            )

        # Insert zero-width space to allow empty fields
        self.entries = [(e[0] or "\u200b", e[1] or "\u200b") for e in entries]

        # Calculate how many pages are needed.
        self.current_page = 0
        self.page_size = page_size
        pages, left_over = divmod(len(self.entries), self.page_size)
        if left_over:
            pages += 1
        self.maximum_pages = pages
        self.timeout = timeout

        self._paginating = len(entries) > self.page_size

        # List of reactions with their respective functions.
        # Order matters! It matches the order of appearance under the post.
        self._reaction_emojis = [
            (self._backward_emoji, self.previous_page),
            (self._forward_emoji, self.next_page),
        ]

        self._match: Optional[Callable[[None], Awaitable[None]]] = None

    async def paginate(self):
        """Actually paginate the entries and run the interactive loop if necessary."""
        first_page = self._show_page(1, first=True)
        if not self._paginating:
            await first_page
        else:
            # Allow us to react to reactions right away if we're paginating.
            self._bot.loop.create_task(first_page)

        while self._paginating:
            try:
                reaction, user = await self._bot.wait_for(
                    "reaction_add", check=self._react_check, timeout=self.timeout
                )
            except asyncio.exceptions.TimeoutError:
                self._paginating = False
                try:
                    # Clear all reactions upon exit.
                    await self._message.clear_reactions()
                except discord.HTTPException:
                    pass
                finally:
                    break

            try:
                await self._message.remove_reaction(reaction, user)
            except discord.HTTPException:
                # Can't remove it so don't bother doing so.
                pass

            # noinspection PyArgumentList
            await self._match()

    async def next_page(self):
        """Flip to the next page."""
        await self._checked_show_page(self.current_page + 1)

    async def previous_page(self):
        """Flip to the previous page."""
        await self._checked_show_page(self.current_page - 1)

    def _get_page(self, page: int):
        base = (page - 1) * self.page_size
        # Quick maffs gets us the next message chunk for our page.
        return self.entries[base : base + self.page_size]

    def _flip_to_page(self, entries: Sequence[Tuple[str, str]], page: int):
        self._embed.clear_fields()

        for key, value in entries:
            self._embed.add_field(name=key, value=value, inline=False)

        if self._paginating:
            page_count = (
                f"Page {to_emoji_case(str(page))} of"
                f" {to_emoji_case(str(self.maximum_pages))}"
                if self.maximum_pages > 1
                else ""
            )
            info = (
                f"React with"
                f" {''.join((self._backward_emoji, self._forward_emoji))}"
                f" to flip page or write the"
                f" desired page name in chat within {int(self.timeout)} seconds."
            )

            self._embed.set_footer(
                text=f"{page_count}{info}",
                # Information icon from twitter's emoji.
                icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com"
                "/thumbs/120/twitter/233/information-source_2139.png",
            )

    async def _checked_show_page(self, page: int):
        if page != 0 and page <= self.maximum_pages:
            await self._show_page(page)

    async def _show_page(self, page: int, *, first=False):
        self.current_page = page
        entries = self._get_page(page)
        self._flip_to_page(entries, page)

        if not self._paginating:
            # Set last message. This allows us to delete it more easily.
            self._message = await self._channel.send(embed=self._embed)
            return self._message

        if not first:
            # Message exists, edit instead of posting another one.
            await self._message.edit(embed=self._embed)
            return

        self._message = await self._channel.send(embed=self._embed)
        for (reaction, _) in self._reaction_emojis:
            # Assign our reactions.
            await self._message.add_reaction(reaction)

    def _react_check(self, reaction: discord.Reaction, user: discord.User):
        if user is None or user.id != self._author.id:
            return False

        if reaction.message.id != self._message.id:
            return False

        for (emoji, func) in self._reaction_emojis:
            if reaction.emoji == emoji:
                # Cheeky function assignment.
                self._match = func
                return True

        return False


# Code below this point is courtesy of Nully. <3
class TabularData:
    def __init__(self):
        self._widths = []
        self._columns = []
        self._rows = []

    def set_columns(self, columns):
        self._columns = columns
        self._widths = [len(c) + 2 for c in columns]

    def add_row(self, row):
        rows = [str(r) for r in row]
        self._rows.append(rows)
        for index, element in enumerate(rows):
            width = len(element) + 2
            if width > self._widths[index]:
                self._widths[index] = width

    def add_rows(self, rows):
        for row in rows:
            self.add_row(row)

    def render(self):
        """
        Example:
        +-------+-----+
        | Memes  | Ye |
        +-------+-----+
        | Alice | 24  |
        |  Bob  | 19  |
        +-------+-----+
        """

        sep = "+".join("-" * w for w in self._widths)
        sep = f"+{sep}+"

        to_draw = [sep]

        def get_entry(d):
            elem = "|".join(f"{e:^{self._widths[i]}}" for i, e in enumerate(d))
            return f"|{elem}|"

        to_draw.append(get_entry(self._columns))
        to_draw.append(sep)

        for row in self._rows:
            to_draw.append(get_entry(row))

        to_draw.append(sep)
        return "\n".join(to_draw)


def cleanup_code(content):
    """Remove code blocks"""
    # remove ```py\n```
    if content.startswith("```") and content.endswith("```"):
        return "\n".join(content.split("\n")[1:-1])

    # remove `foo`
    return content.strip("` \n")


def format_table(results):
    table = TabularData()
    table.set_columns(list(results[0].keys()))
    table.add_rows(list(r.values()) for r in results)
    return table.render()


def paginate_string(
    content: str,
    page_length: int,
    *,
    separator: str = " ",
    enforce_length: bool = False,
) -> List[str]:
    """Split a string into parts of the specified maximum size in chracters. `separator`
    is a string indicating where it's ok to split the string. A Falsy separator
    splits the string anywhere.
    Separators that are rare in the string may result in pages longer than `page_length`
    unless `enforce_length` is True.
    """

    def limit_string(string: str) -> str:
        """Split a string into pages until less than the limit remains and return it."""
        if len(string) >= page_length:
            pages.append(string[:page_length])
            string = string[page_length:]

        return string

    if separator:
        content = content.split(separator)

    pages = []
    page = ""
    for part in content:
        if enforce_length:
            page = limit_string(page)

        if not page:
            page = part
            continue

        if len(page) + 2 + len(part) > page_length:
            pages.append(page)
            page = part
            continue

        page += f"{separator if separator else ''}{part}"

    if page:
        if enforce_length:
            while len(page) >= page_length:
                page = limit_string(page)

        if page:
            pages.append(page)

    return pages
