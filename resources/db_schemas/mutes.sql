SET default_tablespace = '';

CREATE TABLE IF NOT EXISTS public.mutes (
    id bigint PRIMARY KEY,
    server bigint NOT NULL,
    muted_user bigint NOT NULL,
    unmute_at timestamp with time zone NULL
);