import asyncio
import re
import string
from functools import reduce
from typing import Optional, Tuple, Union
import discord
from discord.ext import tasks
from discord.ext.commands import (
    clean_content,
    Cog,
    has_permissions,
    is_owner,
    UserInputError,
)
from berzibot import Berzibot
from components import utils
from components.berzicommand import berzi_group
from components.context import BerziContext
from components.localisation import fetch_line
from components.log import logger

punctuation = set(string.punctuation).difference(set("[]{}()<>"))
"""Punctuation characters except parentheses."""


def setup(bot: Berzibot):
    bot.add_cog(ThingOfTheDay(bot))


class ThingFormatter:
    """Format thing of the day data as either a regular message or an embed."""

    __slots__ = []

    @staticmethod
    def format_entry(
        qualifier_string: str, content_string: str, extra_string: str
    ) -> str:
        """Format a message to publish an entry given its parts."""
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"

        if extra_string and extra_string[-1] not in punctuation:
            extra_string += "."

        return (
            f"{qualifier_string} of the day: "
            f"**{content_string}**"
            f"{', ' + extra_string if extra_string else '.'}\n\n"
            f"ℹ️ {fetch_line('totd footer')}"
        )

    @staticmethod
    def embed_entry(
        qualifier_string: str,
        content_string: str,
        extra_string: str,
        guild: discord.Guild,
    ) -> discord.Embed:
        embed = discord.Embed()

        # Keep acronyms as-is, force title case otherwise.
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"
        embed.title = f"{qualifier_string} of the day"

        embed.set_thumbnail(url=guild.icon_url)

        if extra_string:
            if extra_string[-1] not in punctuation:
                extra_string += "."
        else:
            extra_string = "\N{ZERO WIDTH SPACE}"  # Circumvent empty field error.

        embed.add_field(
            name=f"> **{content_string}**", value=f"{extra_string}", inline=False
        )

        embed.set_footer(
            text=fetch_line("totd footer"),
            # Information icon from twitter's emoji.
            icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com"
            "/thumbs/120/twitter/233/information-source_2139.png",
        )

        return embed


class ThingOfTheDay(Cog):
    """Hold all the commands for thing of the day services."""

    def __init__(self, bot: Berzibot):
        self.bot = bot
        self.publish_totd.add_exception_type(Exception)
        self.publish_totd.add_exception_type(BaseException)
        self.publish_totd.start()

    def cog_unload(self):
        self.publish_totd.cancel()

    @staticmethod
    def _build_query(ctx: BerziContext, **kwargs) -> Tuple[str, list]:
        """Assemble a WHERE clause for a query with the provided columns and values.

        Context is used to add the server (guild) to the query.
        Argument names remain unchanged, thus they are case-sensitive.
        "language" is automatically lowercased.
        "channel" and other Discord entities with an id should be provided as instances
        of said entities, the id is automatically extracted.

        :return: a tuple of the query string and a list of the respective values.
        """

        query_names = ["server"]
        query_values = [ctx.guild.id]

        mapping = {
            "channel": lambda x: x.id,
            "author": lambda x: x.id,
            "language": lambda x: x.lower(),
        }

        for column, value in kwargs.items():
            if not value:
                continue

            query_names.append(column)
            transformer = mapping.get(column, lambda x: x)
            query_values.append(transformer(value))

        query_string = " AND ".join(
            f"{name}=${i}" for i, name in enumerate(query_names, 1)
        )

        return query_string, query_values

    # noinspection PyCallingNonCallable
    @tasks.loop(minutes=1.0)
    async def publish_totd(self):
        """Publish all Things of the Day scheduled for the current minute, if any."""
        current_timeofday = utils.current_timeofday()

        async with self.bot.pool.acquire() as conn:
            entries = await conn.fetch(
                """
with selections as (
    select te.language_id,
           tm.id tm_id,
           te.id te_id,
           name as language,
           te.created_at,
           qualifier,
           content,
           extra,
           author,
           tt.server,
           tt.channel,
           tt.mode,
           (SELECT COUNT(*) FROM totd_mentions tm WHERE tm.entry_id=te.id) AS appeared
    from totd_languages
             join totd_entries te on totd_languages.id = te.language_id
             left join totd_mentions tm on te.id = tm.entry_id
             join totd_tasks tt on te.language_id = tt.language_id
    WHERE is_active=true
    AND extract(hour FROM send_at at time zone 'utc')::int=$1
    AND extract(minute FROM send_at at time zone 'utc')::int=$2
),
entry as (
    select language_id, (select bool_or(a is null) from unnest(checker) s(a)) has_no_appearance
    from (
        select language_id, array_agg(tm_id order by 1 nulls first) checker
        from selections
        group by language_id
    ) sub
)
select distinct on (sub.language_id) te_id as id, qualifier, content, extra, server, channel, mode, appeared, language
from (
    select language_id,
    (
        select te_id
        from selections
        where language_id = entry.language_id
        and ((entry.has_no_appearance and tm_id is null)
        or (not entry.has_no_appearance and tm_id is not null))
        order by random()
        limit 1
    ) joined_id
    from entry
    ) sub
join selections on selections.te_id = joined_id;
                """,
                current_timeofday.hour,
                current_timeofday.minute,
            )

        for entry in entries:
            entry_id = entry["id"]
            qualifier = entry["qualifier"]
            content = entry["content"]
            extra = entry["extra"]
            server_id = entry["server"]
            channel_id = entry["channel"]
            display_mode = entry["mode"]

            # Record mention
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
INSERT INTO totd_mentions
(entry_id, channel, server)
VALUES($1, $2, $3);
                    """,
                    entry_id,
                    channel_id,
                    server_id,
                )

            # Publish entry
            guild = self.bot.get_guild(server_id)
            location = guild.get_channel(channel_id)

            if display_mode == "embed":
                embed = ThingFormatter.embed_entry(qualifier, content, extra, guild)
                embed.colour = self.bot.user.colour
                await location.send(embed=embed)
            else:
                message = ThingFormatter.format_entry(qualifier, content, extra)
                await location.send(message)

            logger.info(f"Published {entry_id=} to {channel_id=} {server_id=}.")

    @publish_totd.before_loop
    async def check_ready(self):
        await self.bot.wait_until_ready()

    @berzi_group(name="totd", hidden=True, meta={"mod_only": True})
    async def thing_of_the_day(self, ctx: BerziContext):
        """Commands for the "thing of the day" service."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd"))

    # TEST
    @thing_of_the_day.berzi_command(name="remaining", meta={"mod_only": True})
    async def count_remaining(self, ctx: BerziContext, *, language: clean_content):
        if not language:
            raise UserInputError

        async with self.bot.pool.acquire() as conn:
            count = await conn.fetch(
                """
SELECT (
    SELECT COUNT(*) FROM (
        SELECT COUNT(*) AS count
        FROM totd_mentions m
        JOIN totd_entries e ON e.id=m.entry_id
        WHERE m.server=$2
        AND m.entry_id=e.id
        AND e.language_id=(
            SELECT id FROM totd_languages WHERE name=$1 LIMIT 1
        )
    ) AS mc
    GROUP BY count
    ORDER BY count
) AS total
LIMIT 1;
                """,
                language.lower(),
                ctx.guild.id,
            )

        total = count["total"][0]

        if not total:
            await ctx.send(fetch_line("fail find", language))
            return

        # TODO actually get number of entries AND number of appearances
        await ctx.send(f"There are {total} remaining entries.")

    @thing_of_the_day.berzi_command(name="sample", meta={"mod_only": True})
    async def sample(
        self, ctx: BerziContext, language: int = None, format_type: str = "embed",
    ):
        """Sample a random entry. `format_type` can be `embed` or `text`."""
        if format_type not in ("text", "embed"):
            raise UserInputError

        subquery = "WHERE language_id=$1" if language is not None else ""
        arguments = [language] if language is not None else []

        async with self.bot.pool.acquire() as conn:
            entry = await conn.fetch(
                f"SELECT"
                f" qualifier,"
                f" content,"
                f" extra"
                f" FROM totd_entries {subquery}"
                f" ORDER BY random()"
                f" LIMIT 1;",
                *arguments,
            )

        for qualifier, content, extra in entry:
            guild = self.bot.get_guild(ctx.guild.id)
            location = guild.get_channel(ctx.channel.id)

            if format_type == "text":
                text = ThingFormatter.format_entry(qualifier, content, extra)
                await location.send(text)
            else:
                embed = ThingFormatter.embed_entry(qualifier, content, extra, guild)
                await location.send(embed=embed)

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="setup", meta={"mod_only": True})
    async def setup(
        self,
        ctx,
        language: clean_content,
        channel: Union[discord.TextChannel, str],
        *,
        send_at: utils.time_converter = None,
    ):
        """Set up a task for issuing things of the day in a channel.
        `language` can be the name for a new one to create.
        `channel` can be `here`.
        `send_at` is the time of day at which to send the next issue; if empty, uses the
        current time.
        """

        send_at = send_at or utils.current_timeofday()
        channel = ctx.channel if channel == "here" else channel

        # Fetch the language
        language = language.lower()
        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        # If not exists: ask confirmation to create (if owner)
        new_language = False
        if not language_id:
            if not await self.bot.is_owner(ctx.author):
                await ctx.send(fetch_line("unknown", language.capitalize()))
                return

            message = fetch_line("unknown should", language.capitalize())
            async with ctx.ask(message, boolean=True) as confirm:
                if not confirm:
                    await ctx.send(fetch_line("cancelled"))
                    return

            await ctx.db.execute(
                "INSERT INTO totd_languages (name) VALUES($1);", language
            )

            new_language = True

            await ctx.send(fetch_line("successful"))
            logger.info(
                f"New {language=} created by {ctx.author} "
                f"in {channel=} {ctx.guild=}."
            )

        # Check whether language is already set up here
        if not new_language:
            existing_tasks = await ctx.db.fetch(
                "SELECT id, send_at, is_active"
                " FROM totd_tasks"
                " WHERE language_id=$1"
                " AND channel=$2"
                " AND server=$3",
                language_id,
                channel.id,
                ctx.guild.id,
            )

            if existing_tasks:
                for task in existing_tasks:
                    if task["send_at"] == send_at:
                        # If set up but inactive, prompt to activate
                        if task["is_active"] is True:
                            await ctx.send(fetch_line("task active"))
                            return

                        message = fetch_line("task at for", send_at, language)
                        async with ctx.ask(message, boolean=True) as confirm:
                            if not confirm:
                                await ctx.send(fetch_line("cancelled"))
                                return

                            await ctx.db.execute(
                                "UPDATE totd_tasks SET is_active=true WHERE id=$1;",
                                task["id"],
                            )

                            await ctx.send(fetch_line("successful"))

                            logger.info(
                                f"Task {task['id']} for language {language}"
                                f" enabled by {ctx.author} "
                                f"in {channel}@{ctx.guild}."
                            )

                        return

        # Set up task here.
        await ctx.db.execute(
            "INSERT INTO totd_tasks (language_id, send_at, channel, server) VALUES($1, $2, $3, $4);",
            language_id,
            send_at,
            channel.id,
            ctx.guild.id,
        )

        await ctx.send(fetch_line("successful"))
        logger.info(
            f"Task for {language=} set up by {ctx.author=} in {channel=}@{ctx.guild=} for {send_at=}."
        )

    async def _toggle_task_for_language(
        self,
        ctx: BerziContext,
        enable: bool,
        *,
        language: str,
        channel: discord.TextChannel,
        send_at: utils.time_converter,
    ):
        query_string, query_values = self._build_query(
            ctx, language=language, channel=channel, send_at=send_at
        )

        count = await ctx.db.execute(
            "UPDATE totd_tasks SET is_active=$1 WHERE " + query_string, *query_values
        )[-1]

        mode = "Enabled" if enable else "Disabled"
        await ctx.send(
            f"{mode} {count} task{'s' if count != 1 else ''}!"
            f" {':D' if count > 0 else ':0'}"
        )

        if count:
            logger.info(
                f"{ctx.author}"
                f" {mode.lower()} {count} task{'s' if count != 1 else ''}"
                f" with {query_string=} {query_values=}."
            )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="enable", meta={"mod_only": True})
    async def enable(
        self,
        ctx: BerziContext,
        language: str,
        send_at: utils.time_converter = None,
        *,
        channel: discord.TextChannel = None,
    ):
        """Enable one or more tasks for the specified language, time, or channel."""
        await self._toggle_task_for_language(
            ctx, True, language=language, channel=channel, send_at=send_at
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="disable", meta={"mod_only": True})
    async def disable(
        self,
        ctx: BerziContext,
        language: str,
        send_at: utils.time_converter = None,
        *,
        channel: discord.TextChannel = None,
    ):
        """Disable one or more tasks for the specified language, time, or channel."""
        await self._toggle_task_for_language(
            ctx, False, language=language, channel=channel, send_at=send_at
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_group(name="list", hidden=True)
    async def list(self, ctx: BerziContext):
        """Commands to list existing elements such as languages or tasks."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd list"))

    @list.berzi_command(name="languages", meta={"mod_only": True})
    async def list_languages(self, ctx, *, search: str = None):
        """List existing languages for Thing of the Day. Optionally `search` for a language name."""
        async with ctx.typing():
            if search:
                search = search.lower()

                results = await ctx.db.fetch(
                    "SELECT name FROM totd_languages"
                    " WHERE name ILIKE '%'|| $1 || '%';",
                    search,
                )
            else:
                results = await ctx.db.fetch("SELECT name FROM totd_languages;")

        if not results:
            extra = " matching" if search else ""
            await ctx.send(fetch_line("fail find language", extra))
            return

        result_string = "\n".join(result["name"] for result in results)

        await ctx.send(result_string)

    @list.berzi_command(name="tasks")
    async def list_tasks(
        self,
        ctx: BerziContext,
        channel: Optional[discord.TextChannel] = None,
        *,
        language: str = None,
    ):
        """List or `search` for "thing of the day" services."""
        channel = channel or ctx.channel

        query_string, query_values = self._build_query(
            ctx, **{"channel": channel, "language": language,}
        )

        async with ctx.typing():
            results = await ctx.db.fetch(
                "SELECT"
                " l.name AS language,"
                " send_at AS time,"
                " server,"
                " channel,"
                " is_active AS active,"
                " t.created_at AS created"
                " FROM totd_tasks t JOIN totd_languages l ON l.id=t.language_id"
                f" WHERE {query_string};",
                *query_values,
            )

        if not results:
            await ctx.send(fetch_line("no match"))
            return

        # Preprocess items
        transformer = {
            "language": lambda x: x.capitalize(),
            "time": lambda x: x.strftime("%H:%M"),
            "channel": lambda x: ctx.guild.get_channel(x).name,
            "active": lambda x: "Yes" if x else "No",
            "created": lambda x: x.strftime("%Y-%m-%d %H:%M %Z"),
        }

        # Determine column headers and ordering
        headers = {
            "language": "Language",
            "time": "Time (server)",
            "channel": "Channel",
            "active": "Active",
            "created": "Created",
        }

        records = [headers]
        records.extend(
            [
                {
                    k: transformer.get(k, lambda x: str(x))(v)
                    for k, v in r.items()
                    if k in headers.keys()
                }
                for r in results
            ]
        )

        # Calculate max width of each column
        widths = {}
        for field, header in headers.items():
            widths[field] = max(
                reduce(
                    lambda x, y: max(len(str(x)), len(str(y))),
                    map(lambda r: r[field], records),
                ),
                len(header),
            )

        # Pad columns
        records = [
            {k: v.ljust(widths.get(k, 0)) for k, v in r.items()} for r in records
        ]

        padding_amount = 3
        padding = " " * padding_amount

        lines = [padding.join(record.values()) for record in records]
        output = "\n".join(lines)
        await ctx.send(f"```\n{output}```")

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_group(name="add", hidden=True, meta={"mod_only": True})
    async def add(self, ctx: BerziContext):
        """Commands to add new elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd add"))

    @is_owner()
    @add.berzi_command(name="language", hidden=True)
    async def add_language(self, ctx, *, language: clean_content):
        """Add a language to which entries can be added."""
        language = language.lower()

        existing = await ctx.db.fetch(
            "SELECT * FROM totd_languages WHERE name=$1;", language
        )

        if existing:
            await ctx.send(fetch_line("already exists", language.capitalize()))
            return

        await ctx.db.execute("INSERT INTO totd_languages(name) VALUES($1);", language)

        await ctx.send(fetch_line("successful"))

        logger.info(f"{ctx.author} added {language=}.")

    @add.berzi_command(name="entry", aliases=["entries"], meta={"mod_only": True})
    async def add_entry(
        self, ctx, language: Optional[str] = None, *, input_string: clean_content = None
    ):
        """Add an entry to a language. Empty arguments, are prompted interactively."""
        if not language:
            try:
                async with ctx.ask(fetch_line("what language")) as answer:
                    language = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        if "|" in language:
            # User inputted entry before language.
            raise UserInputError

        language = language.lower()
        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        if not language_id:
            await ctx.send(fetch_line("fail find", language.capitalize()))
            return

        if not input_string:
            message = (
                "And what do I add?\n"
                "*The format is `qualifier|content(|extra)`, one per line.*"
            )
            try:
                async with ctx.ask(message) as answer:
                    input_string = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        lines = input_string.split("\n")
        dupes = 0
        async with ctx.typing():
            existing_entries = await ctx.db.fetch("SELECT content FROM totd_entries;")
            existing_entries = {r[0] for r in existing_entries}
            entries = []
            for line in lines:
                entry = line.split("|", maxsplit=2)
                if entry[1] in existing_entries:
                    dupes += 1
                    continue

                if not entry[2:]:
                    entry.insert(2, "")
                entries.append(entry)

            await ctx.db.copy_records_to_table(
                "totd_entries",
                records=[(*entry, language_id, ctx.author.id) for entry in entries],
                columns=("qualifier", "content", "extra", "language_id", "author"),
            )

        if (n_entries := len(entries)) == 1:
            await ctx.send(fetch_line("successful"))
        else:
            dupe_sentence = (
                f" (I skipped {dupes}" f" duplicate{'' if dupes == 1 else 's'})"
                if dupes
                else ""
            )
            await ctx.send(
                f"Added {n_entries if n_entries else 'no'}" f" entries{dupe_sentence}."
            )
        logger.info(f"{ctx.author} added {n_entries=} to {language_id=}.")

    @is_owner()
    @thing_of_the_day.berzi_group(name="remove", hidden=True)
    async def remove(self, ctx: BerziContext):
        """Commands to remove elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd remove"))

    @remove.berzi_command(name="language", hidden=True)
    async def remove_language(self, ctx, *, language: clean_content):
        """Remove a given language and all of its entries."""
        language = language.lower()

        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        if not language_id:
            await ctx.send(fetch_line("unknown", language.capitalize()))
            return

        message = fetch_line("remove language", language.capitalize())
        async with ctx.ask(message, boolean=True) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        await ctx.db.execute("DELETE FROM totd_languages WHERE id=$1;", language_id)

        await ctx.send(fetch_line("successful"))

        logger.info(f"{ctx.author} removed {language=}.")

    @remove.berzi_command(name="entry", hidden=True)
    async def remove_entry(self, ctx, *, entry_content: clean_content):
        """Remove an entry from its language."""
        if not entry_content:
            await ctx.send(fetch_line("need entry name"))
            return

        async with ctx.typing():
            entries = await ctx.db.fetch(
                """
SELECT te.id, te.content, tl.name AS language FROM totd_entries te
JOIN totd_languages tl ON te.language_id=tl.id
WHERE content ILIKE '%' || $1 || '%';
                """,
                entry_content,
            )

        if not entries:
            await ctx.send(fetch_line("fail find", entry_content))
            return

        to_remove = []
        if len(entries) > 1:
            entry_list = "\n".join(
                f"{entry['id']} {entry['content']} ({entry['language']})"
                for entry in entries
            )
            message = (
                f"I found all these entries:\n" f"{entry_list}\n" "Which do I remove?"
            )

            try:
                async with ctx.ask(message).lower() as answer:
                    if answer == "all":
                        to_remove = [entry["id"] for entry in entries]
                    else:
                        answer = [s.lower().strip() for s in answer.split(",")]
                        to_remove.extend(
                            [
                                entry_id
                                for entry_id in map(
                                    lambda e: e["id"],
                                    filter(
                                        lambda e: (
                                            e["id"] in answer
                                            or e["content"] in answer
                                            or e["language"] in answer
                                        ),
                                        entries,
                                    ),
                                )
                            ]
                        )

            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return
        else:
            to_remove = [entries[0]["id"]]

        async with ctx.ask(fetch_line("sure"), boolean=True) as answer:
            if not answer:
                await ctx.send(fetch_line("cancelled"))
                return

        async with ctx.typing():
            await ctx.db.execute(
                "DELETE FROM totd_entries WHERE id=ANY($1::INT[]);", to_remove
            )

        await ctx.send(fetch_line("successful"))
        logger.info(f"{ctx.author} removed entries {to_remove}.")

    @is_owner()
    @thing_of_the_day.berzi_group(name="edit", hidden=True)
    async def edit(self, ctx: BerziContext):
        """Commands to modify elements such as languages."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd edit"))

    @edit.berzi_command(name="language", hidden=True)
    async def edit_language(self, ctx: BerziContext, language: str, *, new_name: str):
        """Edit the name of a given language."""
        language = language.lower()
        new_name = new_name.lower()

        await ctx.db.execute(
            "UPDATE totd_languages SET name=$1 WHERE name=$2", new_name, language
        )

        await ctx.send(fetch_line("successful"))
        logger.info(f"{ctx.author} changed the name of {language=} to {new_name=}.")

    @thing_of_the_day.berzi_group(name="count", hidden=False)
    async def count(self, ctx: BerziContext):
        """Commands to count elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd count"))

    @count.berzi_command(name="languages")
    async def count_languages(self, ctx: BerziContext):
        """Count how many languages are configured for "thing of the day"."""
        count = await ctx.db.fetchval("SELECT COUNT(*) FROM totd_languages;")

        if count > 1:
            extra = " I'm pretty smart aren't I?"
        elif count < 1:
            extra = " I feel stoopid."
        else:
            extra = ""

        await ctx.send(f"I know {count} languages.{extra}")

    @count.berzi_command(name="entries")
    async def count_entries(self, ctx: BerziContext, *, languages: str = None):
        """Count how many entries there are for any configured language (separated by comma)."""
        query = lang_exists_doubt = ""
        language_string = "all languages"
        args = []
        languages_no = 0
        if languages is not None:
            languages = re.split(r",\s*", languages)
            if (languages_no := len(languages)) == 1:
                language_string = languages[0].capitalize()
            else:
                language_string = ", ".join(
                    lang.capitalize() for lang in languages[:-1]
                )
                language_string += f" and {languages[-1].capitalize()} together"

            query = " JOIN totd_languages l ON language_id=l.id WHERE "
            query += " OR ".join(f"l.name=${idx}" for idx in range(1, languages_no + 1))
            args.extend(languages)

        count = await ctx.db.fetchval(
            "SELECT COUNT(*) FROM totd_entries " f"{query};", *args
        )

        if count == 0:
            lang_exists_doubt = f", if {'it even exists' if languages_no == 1 else 'if they even exist'}"

        await ctx.send(f"{count} entries for {language_string}{lang_exists_doubt}.")
